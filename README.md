
# Machine Learning for semi linear PDEs


Quentin CHAN-WAI-NAM, Joseph MIKAEL, Xavier WARIN


Implements algorithms used in the article https://arxiv.org/pdf/1809.07609.pdf. 
This code is a work in progress and will be cleaned and enhanced in the future.


### Structure of the project


Main package implementing DBSDE: `deep_bsde`, inside of which we find:
* `networks`: contains neural networks (w/ option to use the same or different networks for each timestep)
* `pdes`: implements PDEs and corresponding functions
* `solver`: put everything together and handle SGD


Main package implementing our new machine learning algorithm: `deep_nesting`, inside of which we find:
* `pdes`: implements PDEs and corresponding functions
* `solver_base`: base functions for solvers
* `solver`: defining solvers with different networks

The user should define its own PDE in the `pdes` file taking as example the existing PDEs. Be careful, as in the current version, the definition is not the same for `deep_bsde` and `deep_nesting` (dimensions are switched between the two algorithms).


### Usage


The user can find several use examples as Python files. You can run our snippets using Python 3, e.g.:
```
python example_bsbarenblatt_deep_bsde.py
```


### Dependancies


We advise using [Anaconda](https://www.anaconda.com/): download and install it (we used Python 3), then in the Anaconda Prompt (or Terminal on Linux systems):
```
conda install tensorflow-gpu scipy matplotlib joblib
```
or, if using a non-GPU system:
```
conda install tensorflow scipy matplotlib joblib
```

### PDEs


The PDEs from our article can be found in the code as follows:


| Article | Code                    |
|---------|-------------------------|
| A.1.1   | BlackScholesDefaultRisk |
| A.1.2   | BlackScholesBarenblatt  |
| A.1.3   | HamiltonJacobiBellman   |
| A.1.4   | SecondToyExample        |
| A.1.5   | RichouToyExample        |
| A.1.6   | CIRToy                  |
| A.1.7   | ToyExample_ysz          |


#### Monte-Carlo solutions


Equations A.1.3 and A.1.5 have known Monte-Carlo solutions. The code implement some functions to compute these solutions, however this is costly and requires a lot of memory and time, and thus should not be run in a consumer grade computer for `d > 10`.


We provide the solutions we used for our tests at [this URL](https://drive.google.com/open?id=1D-WWfSMLe6JdvsgOndRUPS-U99rfwLYj).


In order to use them with our code, you need to put the files at the following path: 


`<repo root>/mc_solutions/`


For instance: 


`<repo root>/mc_solutions/HJB_Y_75e1f87688b763223a62af93f468c104_50000`


Our code will fetch the desired MC solution automatically, but you need to use `N=100` timesteps, and the standard parameters as described in our article. Also, include the following parameters:


```
# For deep_bsde
sp = dbsde.SimpleSolver.Parameters(...)
sp.override_mc=True
sp.override_mc_size=50000

# For deep_nesting
sp = dnesting.XavierSolver.Parameters(...)
sp.override_mc=True
sp.override_mc_size=50000
```


Example scripts are available: `example_richou_deep_nesting.py`, `example_richou_deep_bsde.py`.


### Algorithms


#### Deep BSDE


Package: `deep_bsde`


A Deep BSDE algorithm consists in the combination of a `Solver` class and a `Network` class. For the last LSTMs, we hardcoded the networks in the solver for convenience. The corresponding combinations are:


| Algorithm | Name               | Solver class                       | Network class                     |
|-----------|--------------------|------------------------------------|-----------------------------------|
| a.        | FC DBSDE           | SimpleSolver                       | SimpleNetwork                     |
| b.        | FC ELU             | SimpleSolver                       | SimpleNetwork                     |
| c.        | FC Residual        | SimpleSolver_gX                    | SimpleNetwork_Vertical_Residual   |
| d.        | FC Merged          | SimpleSolver_Recurrent_Y_gX        | SingleWeights_Vertical_ConcatTime |
| e.        | FC Merged Shortcut | SimpleSolver_Recurrent_Y_gX        | SingleWeights_Vertical_Shortcut   |
| f.        | FC Merged Residual | SimpleSolver_Recurrent_Y_gX        | SingleWeights_Vertical_Residual   |
| g.        | LSTM               | SimpleSolver                       | LSTMBlockFusedCell_ConcatTime     |
| h.        | Augmented LSTM     | Solver_RecurrentLSTM_deep          | -                                 |
| i.        | Hybrid LSTM        | Solver_RecurrentLSTM_deep_hybrid   | -                                 |
| j.        | Residual LSTM      | Solver_RecurrentLSTM_deep_residual | -                                 |


#### Deep nesting


Package: `deep_nesting`


This corresponds to our new algorithm. Each algorithm consists in calling an instance of `Solver` with:


| Algorithm | Code                                                                         |
|-----------|------------------------------------------------------------------------------|
| A.        | XavierSolver_NetworkForDU                                                    |
| B.        | XavierSolver_NetworkForDU_shared                                             |
| Cbis.     | XavierSolver_NoCostOnDU if f depends on Z, or XavierSolver_NoDU else         |
| C.        | XavierSolver                                                                 |

