##
# Example use of dnesting on an equation with an oscillating solution and a square non-linearity
# From https://arxiv.org/abs/1809.07609
#

import numpy as np
import tensorflow as tf
import deep_xavier as dbsde
import time
import os
import matplotlib.pyplot as plt


####
# In this example, we solve equation A.1.4 (d=10) using our new algorithm.
# Use example: 
# python example_secondtoyexample_deep_nesting.py
####


r = 0.1

lambd = 0.5
n_inner = 10000

monte_carlo_size = 1024
min_decrease_rate = 0.05

d=10


T = 1.
X0 = np.array([1.0, 0.5]*(int(d/2)), dtype=np.float32)
n_timesteps=100

mu_zero = 0.2
sigma_zero = 1.
a = 0.5

pde = dbsde.SecondToyExample(
        a_mu_zero = mu_zero,
        a_sigma_zero = sigma_zero,
        a_d=d,
        a_T=T,
        a_a=a,
        a_r=r
        )
case_name = "SecondToyExample_d10"

print("----- Solving SecondToyExample -----")

sp = dbsde.XavierSolver.Parameters(
        pde,
        d=d,
        T=T,
        n_timesteps=n_timesteps,
        X0=X0,
        Yini=[.7, 1.3]
        )

sp.valid_size = monte_carlo_size

sp.learning_rate = 0.01
sp.use_predefined_learning_rate_strategy = False
sp.n_miniter = 16000
sp.n_maxiter = 16000

sp.batch_size = 300
sp.valid_size = 1000
sp.normalize_input_X = True

sp.num_hidden_layers = 3
sp.hidden_layer_size = 2*d#3*d
sp.activation_fn = tf.nn.tanh

sp.n_inner = n_inner
sp.lamb = lambd
sp.t_distrib_a = 0.0


solver = dbsde.XavierSolver(sp, base_name=case_name, output_directory="./output/test_deep_xavier_secondtoyexampled10/")

solver.train(run_name="Test", min_decrease_rate=min_decrease_rate)
