##
# Implements networks for DBSDE solvers
# From https://arxiv.org/abs/1809.07609
#

import numpy as np
import tensorflow as tf

import abc

from tensorflow import constant_initializer as cst_init
from tensorflow import random_uniform_initializer as unif_init
from tensorflow import random_normal_initializer as nor_init
from tensorflow.python.training.moving_averages import assign_moving_average
from tensorflow.python.ops import control_flow_ops
from tensorflow.python.util import nest

from tensorflow.contrib.layers import fully_connected as fc
from tensorflow.contrib import rnn 
from tensorflow.contrib.rnn import LSTMBlockFusedCell 

# Setting this flag True, TensorFlow will use dynamic memory allocation when using LSTMs.
# This enables to use very large networks that otherwise would not fit in GPU memory.
SWAP_MEMORY = True


############################################################################################################	
############################################################################################################	
############################################################################################################	


##
# @brief Abstract template for a neural network.
#
class AbstractNetwork(abc.ABC):
	
	def is_recurrent_y_gx(self):
		return False
	
	@abc.abstractmethod
	##
	# @brief Returns whether the network should be built vertically (timestep by timestep) or horizontally (layer by layer).
	#
	# @param self
	def is_vertical(self):
		pass
	
	##
	# @brief Check whether the network also creates a network to compute Z0 from (X0, ...).
	#
	# @param self
	def handles_starting_gradient(self):
		return False
	
	##
	# @brief Gets an initializer for a layer.
	def get_initializer(self, a_in_size, a_out_size, seed=None):
		return nor_init(stddev = 1.41/np.sqrt(a_in_size + a_out_size), seed=seed)

class HorizontalNetwork(AbstractNetwork):
	
	def is_vertical(self):
		return False
	
	@abc.abstractmethod
	##
	# @brief Given an input tensor, build a network and give an output tensor.
	#
	# @param self
	# @param a_input Input tensor for the network
	# @param a_namespace Namespace for the network (e.g. timestep)
	# @param a_training Training indicator tensor. Will be True when training, else false.
	# @param a_timesteps Index of the current timestep.
	# @param reuse Flag to make the network reuse or create variables.
	#
	# @return Output tensor.
	def get_horizontal_network(
			self, 
			a_input, 
			a_namespace, 
			a_training, 
			a_timesteps, 
			reuse
			):
		pass

class VerticalNetwork(AbstractNetwork):
	
	def is_vertical(self):
		return True
	
	@abc.abstractmethod
	##
	# @brief Given an input tensor, build a network and give an output tensor.
	#
	# @param self
	# @param a_input Input tensor for the network
	# @param a_namespace Namespace for the network (e.g. timestep)
	# @param a_training Training indicator tensor. Will be True when training, else false.
	# @param a_timesteps Index of the current timestep.
	# @param reuse Flag to make the network reuse or create variables.
	#
	# @return Output tensor.
	def get_vertical_network(
			self, 
			a_input, 
			a_namespace, 
			a_training, 
			a_timestep, 
			reuse
			):
		pass

class RecurrentNetwork_Y_gX(AbstractNetwork):
	
	def is_vertical(self):
		return True
	
	def is_recurrent_y_gx(self):
		return True
	
	def handles_starting_gradient(self):
		return False
	
	@abc.abstractmethod
	##
	# @brief Given an input tensor, build a network and give an output tensor.
	#
	def get_recurrent_network(
			self, 
			a_Xt,
			a_Xtm1,
			a_Y,
			a_gX,
			a_namespace, 
			a_training, 
			a_timestep, 
			reuse
			):
		pass



############################################################################################################	
############################################################################################################	
############################################################################################################	


##
# @brief The network returning Z:=0, used for sanity check
#
class ZeroNetwork(AbstractNetwork):
	
	def __init__(self):
		pass
	
	def is_vertical(self):
		return False
	
	def handles_starting_gradient(self):
		return True
	
	def get_horizontal_network(
			self, 
			a_input, 
			a_namespace, 
			a_training, 
			a_timesteps, 
			reuse
			):
		zero_line = tf.zeros([1, tf.shape(a_input)[1], tf.shape(a_input)[2]])
		return tf.tile(zero_line, [tf.shape(a_input)[0], 1, 1])



############################################################################################################	
############################################################################################################	
############################################################################################################



##
# @brief Get a fully connected network, as in Jentzen et al.
#
class SimpleNetwork(VerticalNetwork):
	
	def __init__(self, output_size, hidden_layer_sizes=[], activation_fn=tf.nn.relu, batch_normalization_flag=True):
		
		self.output_size = output_size
		self.hidden_layer_sizes = hidden_layer_sizes
		self.activation_fn = activation_fn
		self.batch_normalization_flag = batch_normalization_flag
	
	##
	# @brief For this network, returns a new fully connected network (create layers etc).
	def get_vertical_network(
			self, 
			a_input, 
			a_namespace, 
			a_training, 
			a_timestep, 
			reuse
			):
		
		# Create the layers
		with tf.variable_scope(a_namespace, reuse=reuse):
			
			# Input layer.
			layer = a_input
			
			# Hidden layers
			for i in range(len(self.hidden_layer_sizes)):
				layer = self.vertical_layer(
							a_input=layer, 
							a_activation_fn=self.activation_fn,
							a_out_size=self.hidden_layer_sizes[i],
							a_namespace="layer"+str(i),
							a_training=a_training,
							a_batch_normalization=self.batch_normalization_flag,
							reuse=reuse
							)
			
			# Output layer
			# Performs best when not batch normalizing the output layer
			output = self.vertical_layer(
					a_input=layer, 
					a_activation_fn=None,
					a_out_size=self.output_size,
					a_namespace="output_layer",
					a_training=a_training,
					a_batch_normalization=False,
					reuse=reuse
					)
		
		return output

	##
	# @brief Defines a hidden layer.
	# Apply the operations out <- activation_fn(batch_normalization(W*x)).
	def vertical_layer(
		self, 
		a_input, 
		a_activation_fn, 
		a_out_size, 
		a_namespace, 
		a_training, 
		a_batch_normalization, 
		reuse,
		center=True,
		scale=True
		):
		
		with tf.variable_scope(a_namespace, reuse=reuse):
			in_size = a_input.get_shape().as_list()[1]
			
			if a_batch_normalization:
				# Return a batch normalized fully connected layer
				x = fc(a_input, a_out_size, activation_fn=a_activation_fn, biases_initializer=None, reuse=reuse, scope="layer")
				
				# Create a batch normalization layer
				bn = tf.layers.BatchNormalization(
					axis=1,
					momentum=0.99,
					epsilon=1e-8,
					center=center,
					scale=scale,
					beta_initializer=nor_init(0.0, stddev=0.1, dtype=tf.float32),
					gamma_initializer=unif_init(0.1, 0.5, dtype=tf.float32),
					moving_mean_initializer=tf.zeros_initializer(),
					moving_variance_initializer=tf.ones_initializer(),
					renorm=False,
					fused=True,
					trainable=True,
					name="bn",
					_reuse=reuse,
					_scope="bn"
					)

				# Apply bn
				res = bn.apply(x, training=a_training)
				
				# If not training, add moving means and vars to save collection
				if not a_training:
					tf.add_to_collection(
						"NOT_TRAINING_VAR_TO_SAVE",
						bn.moving_mean
					)
					tf.add_to_collection(
						"NOT_TRAINING_VAR_TO_SAVE",
						bn.moving_variance
					)
				
				# Apply activation function
				if a_activation_fn is None:
					return res
				else:
					return a_activation_fn(res)
				
			else:
				# Return a fully connected layer
				return fc(a_input, a_out_size, activation_fn=a_activation_fn)
				

############################################################################################################	
############################################################################################################	
############################################################################################################


##
# @brief Get a fully connected network as in Jentzen et al but share all the learnt
# parameters (eg beta, gamma, W) between the timesteps (but not the moving means). 
#
# This is equivalent to assuming the network should learn an identical response in all 
# timesteps, i.e. that Z is function only of (mu_t, sigma_t) (batch moving means and vars). 
#
class SingleWeights(HorizontalNetwork):
	
	def handles_starting_gradient(self):
		return True
	
	def __init__(self, output_size, hidden_layer_sizes=[], activation_fn=tf.nn.relu, batch_normalization_flag=False):
		
		self.output_size = output_size
		self.hidden_layer_sizes = hidden_layer_sizes
		self.activation_fn = activation_fn
		self.batch_normalization_flag = batch_normalization_flag
					
		
	##
	# @brief For this network, returns layers using the shared parameters.
	def get_horizontal_network(
		self, 
		a_input, 
		a_namespace, 
		a_training, 
		a_timesteps, 
		reuse
		):
		
		# Create the layers
		with tf.variable_scope(a_namespace, reuse=reuse):
			
			# Input layer
			layer = a_input
			
			for i in range(len(self.hidden_layer_sizes)-1):
				layer = self.horizontal_layer(
							a_input=layer, 
							a_output_size=self.hidden_layer_sizes[i],
							a_activation_fn=self.activation_fn,
							a_namespace="layer_" + str(i),
							a_training=a_training,
							a_batch_normalization=self.batch_normalization_flag,
							reuse=reuse
							)
			
			# Output layer
			output = self.horizontal_layer(
					a_input=layer, 
					a_output_size=self.output_size,
					a_activation_fn=None,
					a_namespace="output_layer",
					a_training=a_training,
					a_batch_normalization=self.batch_normalization_flag,
					reuse=reuse
					)
	
		
		return output

	##
	# @brief Defines a hidden layer.
	# Apply the operations out <- activation_fn(batch_normalization(W*x)).
	def horizontal_layer(
		self, 
		a_input, 
		a_output_size,
		a_activation_fn,
		a_namespace, 
		a_training,
		a_batch_normalization,
		reuse
		):
			
			
		tmp = tf.transpose(a_input, [2, 0, 1])
		
		if a_batch_normalization:
			tmp = tf.map_fn(lambda x: fc(x, a_output_size, activation_fn=a_activation_fn, scope=a_namespace, reuse=reuse), tmp)
			res =  tf.map_fn(lambda x: tf.layers.batch_normalization(x, training=a_training, reuse=reuse, fused=True, name=a_namespace + "_bn"), tmp)
			if a_activation_fn is not None:
				res = a_activation_fn(res)
		else:
			res = tf.map_fn(lambda x: fc(x, a_output_size, activation_fn=None, scope=a_namespace, biases_initializer=None, reuse=reuse), tmp)
		
		res = tf.transpose(res, [1, 2, 0])
		
		return res
				

############################################################################################################	
############################################################################################################	
############################################################################################################	


##
# @brief Get a fully connected network as in Jentzen et al but share all the learnt
# parameters (eg beta, gamma, W) between the timesteps (but not the moving means). 
#
# Moreover, concatenate the time to the input of the layers (as a supplementary dimension)
#
class SingleWeightsConcatTime(SingleWeights):
	
	##
	# @brief For this network, returns layers using the shared parameters.
	def get_horizontal_network(
		self, 
		a_input, 
		a_namespace, 
		a_training,
		a_timesteps, 
		reuse
		):
		
		# Create timestep tensor
		sample_size = tf.shape(a_input)[0]
		n_timesteps = len(a_timesteps)
		tf_timesteps = tf.constant(a_timesteps, dtype=tf.float32, shape=[1, n_timesteps])
		tstep = tf.expand_dims(tf.tile(tf_timesteps, [sample_size, 1]), axis=1) #+ tf.random_normal([sample_size, 1], stddev=0.01)
		
		# Input layer
		layer = tf.concat([a_input, tstep], axis=1)
			
		output = super().get_horizontal_network(
				a_input=layer,
				a_namespace=a_namespace,
				a_training=a_training,
				a_timesteps=a_timesteps,
				reuse=reuse
			)
		
		return output



############################################################################################################	
############################################################################################################	
############################################################################################################	



##
# @brief Get a simple stacked LSTM
#
#
class BasicLSTM(HorizontalNetwork):
	
	def handles_starting_gradient(self):
		return True
	
	def __init__(self, output_size, state_size=10, num_layers=2, activation=tf.nn.elu, use_peepholes=False):
		self.state_size = state_size
		self.num_layers = num_layers
		self.output_size = output_size
		self.activation = activation
		self.use_peepholes = use_peepholes
	
	##
	# @brief For this network, returns layers using the shared parameters.
	def get_horizontal_network(
		self, 
		a_input, 
		a_namespace, 
		a_training,
		a_timesteps, 
		reuse
		):
		
		# Create the layers
		with tf.variable_scope(a_namespace, reuse=reuse):
			
			sample_size = tf.shape(a_input)[0]
			
			cells = []
			for i in range(self.num_layers):
				cells.append(tf.nn.rnn_cell.LSTMCell(self.state_size, activation=self.activation, use_peepholes=self.use_peepholes, initializer=self.get_initializer(self.state_size, self.state_size)))
			cell = tf.nn.rnn_cell.MultiRNNCell(cells)
			
			initial_states = []
			for i in range(self.num_layers):
				tmpc = tf.get_variable("initial_state_c_"+str(i), [1, self.state_size], tf.float32, initializer=self.get_initializer(self.state_size, 0), trainable=True)
				tmph = tf.get_variable("initial_state_h_"+str(i), [1, self.state_size], tf.float32, initializer=self.get_initializer(self.state_size, 0), trainable=True)
				initial_states.append(tf.tile(tmpc, [sample_size, 1]))
				initial_states.append(tf.tile(tmph, [sample_size, 1]))
			initial_states = nest.pack_sequence_as(structure=cell.state_size, flat_sequence=initial_states)
			print("initial_states", initial_states)
			
			reversed_inputs = tf.transpose(a_input, [0, 2, 1])
			
			rnn_outputs, final_state = tf.nn.dynamic_rnn(cell, reversed_inputs, dtype=tf.float32, initial_state=initial_states)
			
			reversed_outputs = tf.transpose(rnn_outputs, [1, 0, 2])	
			
			W = tf.get_variable('W', [self.state_size, self.output_size], initializer=self.get_initializer(self.state_size, self.output_size))
			b = tf.get_variable('b', [self.output_size], initializer=self.get_initializer(0, self.output_size))
			
			# TODO: this could be done in a better way
			res = tf.map_fn(lambda x: tf.matmul(x, W) + b, reversed_outputs)
			
			res = tf.transpose(res, [1, 2, 0])
			
			return res


############################################################################################################	
############################################################################################################	
############################################################################################################	


##
# @brief Get a simple stacked LSTM
#
class BasicLSTM_ConcatTime(HorizontalNetwork):
	
	def handles_starting_gradient(self):
		return True
	
	def __init__(self, output_size, state_size=10, num_layers=2, activation=tf.nn.elu, use_peepholes=False):
		self.state_size = state_size
		self.num_layers = num_layers
		self.output_size = output_size
		self.activation = activation
		self.use_peepholes = use_peepholes
	
	##
	# @brief For this network, returns layers using the shared parameters.
	def get_horizontal_network(
		self, 
		a_input, 
		a_namespace, 
		a_training,
		a_timesteps, 
		reuse
		):
		
		# Create the layers
		with tf.variable_scope(a_namespace, reuse=reuse):
			
			sample_size = tf.shape(a_input)[0]
			
			cells = []
			for i in range(self.num_layers):
				cells.append(tf.nn.rnn_cell.LSTMCell(self.state_size, activation=self.activation, use_peepholes=self.use_peepholes, initializer=self.get_initializer(self.state_size, self.state_size)))
			cell = tf.nn.rnn_cell.MultiRNNCell(cells)
			
			initial_states = []
			for i in range(self.num_layers):
				tmpc = tf.get_variable("initial_state_c_"+str(i), [1, self.state_size], tf.float32, initializer=self.get_initializer(self.state_size, 0), trainable=True)
				tmph = tf.get_variable("initial_state_h_"+str(i), [1, self.state_size], tf.float32, initializer=self.get_initializer(self.state_size, 0), trainable=True)
				initial_states.append(tf.tile(tmpc, [sample_size, 1]))
				initial_states.append(tf.tile(tmph, [sample_size, 1]))
			initial_states = nest.pack_sequence_as(structure=cell.state_size, flat_sequence=initial_states)
			print("initial_states", initial_states)
			
			print("cell.state_size", cell.state_size)
			
			n_timesteps = len(a_timesteps)
			tf_timesteps = tf.constant(a_timesteps, dtype=tf.float32, shape=[1, n_timesteps])
			tstep = tf.expand_dims(tf.tile(tf_timesteps, [sample_size, 1]), axis=1)
			
			reversed_inputs = tf.transpose(tf.concat([a_input, tstep], axis=1), [0, 2, 1])
			
			rnn_outputs, final_state = tf.nn.dynamic_rnn(cell, reversed_inputs, dtype=tf.float32, initial_state=initial_states)
			
			reversed_outputs = tf.transpose(rnn_outputs, [1, 0, 2])	
			
			W = tf.get_variable('W', [self.state_size, self.output_size], initializer=self.get_initializer(self.state_size, self.output_size))
			b = tf.get_variable('b', [self.output_size], initializer=tf.constant_initializer(0.0))
			
			res = tf.map_fn(lambda x: tf.matmul(x, W) + b, reversed_outputs)
			
			res = tf.transpose(res, [1, 2, 0])
			
			return res


############################################################################################################	
############################################################################################################	
############################################################################################################	



##
# @brief Get a simple stacked LSTM taking as input (t, X)
#
class LSTMBlockFusedCell_ConcatTime(HorizontalNetwork):
	
	def handles_starting_gradient(self):
		return True
	
	##
	# @param output_size The width of the output layer
	# @param state_size The width of the hidden layers
	#
	def __init__(self, output_size, state_size=10, num_layers=2, use_peepholes=False):
		self.state_size = state_size
		self.num_layers = num_layers
		self.output_size = output_size
		self.use_peepholes = use_peepholes
	
	##
	# @brief For this network, returns layers using the shared parameters.
	#
	def get_horizontal_network(
		self, 
		a_input, 
		a_namespace, 
		a_training,
		a_timesteps, 
		reuse
		):
		
		# Create the layers
		with tf.variable_scope(a_namespace, reuse=reuse):
			
			sample_size = tf.shape(a_input)[0]
			
			cells = []
			for i in range(self.num_layers):
				cells.append(LSTMBlockFusedCell(
					num_units=self.state_size,
					forget_bias=1.0,
					cell_clip=None,
					use_peephole=self.use_peepholes
					))
			
			initial_states = []
			for i in range(self.num_layers):
				tmpc = tf.get_variable("initial_state_c_"+str(i), [1, self.state_size], tf.float32, initializer=self.get_initializer(self.state_size, 0), trainable=True)
				tmph = tf.get_variable("initial_state_h_"+str(i), [1, self.state_size], tf.float32, initializer=self.get_initializer(self.state_size, 0), trainable=True)
				initial_states.append(tuple([tf.tile(tmpc, [sample_size, 1]), tf.tile(tmph, [sample_size, 1])]))
			print("initial_states", initial_states)
			
			n_timesteps = len(a_timesteps)
			tf_timesteps = tf.constant(a_timesteps, dtype=tf.float32, shape=[1, n_timesteps])
			tstep = tf.expand_dims(tf.tile(tf_timesteps, [sample_size, 1]), axis=1)
			
			# [n_timesteps, batch_size, d]
			reversed_inputs = tf.transpose(tf.concat([a_input, tstep], axis=1), [2, 0, 1])			
			inputs = reversed_inputs
			
			for i in range(len(cells)):
				outputs, state = cells[i](inputs, initial_state=initial_states[i], dtype=tf.float32, scope="LSTM_layer_" + str(i))
				inputs = outputs
			
			# [n_timesteps, batch_size, d]
			reversed_outputs = outputs

			
			W = tf.get_variable('W', [self.state_size, self.output_size], initializer=self.get_initializer(self.state_size, self.output_size))
			b = tf.get_variable('b', [self.output_size], initializer=tf.constant_initializer(0.0))
			
			res = tf.map_fn(lambda x: tf.matmul(x, W) + b, reversed_outputs)
			
			# [batch_size, d, n_timesteps]
			res = tf.transpose(res, [1, 2, 0])

			return res

############################################################################################################	
############################################################################################################	
############################################################################################################	



##
# @brief Get a simple stacked GRU network taking as input X. This network is less stable than stacked LSTMs.
#
class BasicGRU(HorizontalNetwork):
	
	def handles_starting_gradient(self):
		return True
	
	def __init__(self, output_size, state_size=10, num_layers=2, activation=tf.nn.elu):
		self.state_size = state_size
		self.num_layers = num_layers
		self.output_size = output_size
		self.activation = activation
	
	##
	# @brief For this network, returns layers using the shared parameters.
	#
	def get_horizontal_network(
		self, 
		a_input, 
		a_namespace, 
		a_training,
		a_timesteps, 
		reuse
		):
		
		# Create the layers
		with tf.variable_scope(a_namespace, reuse=reuse):
			
			sample_size = tf.shape(a_input)[0]
			
			cells = []
			for i in range(self.num_layers):
				cells.append(tf.nn.rnn_cell.GRUCell(self.state_size, activation=self.activation))
			cell = tf.nn.rnn_cell.MultiRNNCell(cells)
			
			initial_states = []
			for i in range(self.num_layers):
				tmph = tf.get_variable("initial_state_h_"+str(i), [1, self.state_size], tf.float32, initializer=self.get_initializer(self.state_size, 0), trainable=True)
				initial_states.append(tf.tile(tmph, [sample_size, 1]))
			initial_states = nest.pack_sequence_as(structure=cell.state_size, flat_sequence=initial_states)
			print("initial_states", initial_states)
			
			reversed_inputs = tf.transpose(a_input, [0, 2, 1])			
			
			rnn_outputs, final_state = tf.nn.dynamic_rnn(cell, reversed_inputs, dtype=tf.float32, initial_state=initial_states)
			
			reversed_outputs = tf.transpose(rnn_outputs, [1, 0, 2])	
			
			W = tf.get_variable('W', [self.state_size, self.output_size], initializer=self.get_initializer(self.state_size, self.output_size))
			b = tf.get_variable('b', [self.output_size], initializer=tf.constant_initializer(0.0))
			
			res = tf.map_fn(lambda x: tf.matmul(x, W) + b, reversed_outputs)
			
			res = tf.transpose(res, [1, 2, 0])
			
			return res



############################################################################################################	
############################################################################################################	
############################################################################################################	



##
# @brief Get a simple stacked GRU network taking as input (t, X). This network is less stable than stacked LSTMs.
#
class BasicGRU_ConcatTime(HorizontalNetwork):
	
	def handles_starting_gradient(self):
		return True
	
	def __init__(self, output_size, state_size=10, num_layers=2, activation=tf.nn.elu):
		self.state_size = state_size
		self.num_layers = num_layers
		self.output_size = output_size
		self.activation = activation
	
	##
	# @brief For this network, returns layers using the shared parameters.
	#
	def get_horizontal_network(
		self, 
		a_input, 
		a_namespace, 
		a_training,
		a_timesteps, 
		reuse
		):
		
		# Create the layers
		with tf.variable_scope(a_namespace, reuse=reuse):
			
			sample_size = tf.shape(a_input)[0]
			
			cells = []
			for i in range(self.num_layers):
				cells.append(tf.nn.rnn_cell.GRUCell(self.state_size, activation=self.activation))
			cell = tf.nn.rnn_cell.MultiRNNCell(cells)
			
			initial_states = []
			for i in range(self.num_layers):
				tmph = tf.get_variable("initial_state_h_"+str(i), [1, self.state_size], tf.float32, initializer=self.get_initializer(self.state_size, 0), trainable=True)
				initial_states.append(tf.tile(tmph, [sample_size, 1]))
			initial_states = nest.pack_sequence_as(structure=cell.state_size, flat_sequence=initial_states)
			print("initial_states", initial_states)
			
			n_timesteps = len(a_timesteps)
			tf_timesteps = tf.constant(a_timesteps, dtype=tf.float32, shape=[1, n_timesteps])
			tstep = tf.expand_dims(tf.tile(tf_timesteps, [sample_size, 1]), axis=1)
			
			reversed_inputs = tf.transpose(tf.concat([a_input, tstep], axis=1), [0, 2, 1])
			
			rnn_outputs, final_state = tf.nn.dynamic_rnn(cell, reversed_inputs, dtype=tf.float32, initial_state=initial_states)
			
			reversed_outputs = tf.transpose(rnn_outputs, [1, 0, 2])	
			
			
			W = tf.get_variable('W', [self.state_size, self.output_size], initializer=self.get_initializer(self.state_size, self.output_size))
			b = tf.get_variable('b', [self.output_size], initializer=tf.constant_initializer(0.0))
			
			res = tf.map_fn(lambda x: tf.matmul(x, W) + b, reversed_outputs)
			
			res = tf.transpose(res, [1, 2, 0])
			
			return res



############################################################################################################	
############################################################################################################	
############################################################################################################	


##
# @brief Get a fully connected network as in Jentzen et al but share all the learnt
# parameters (eg beta, gamma, W) between the timesteps (but not the moving means). 
#
# This is equivalent to assuming the network should learn an identical response in all 
# timesteps, i.e. that Z is function only of (mu_t, sigma_t) (batch moving means and vars). 
#
class SingleWeights_Vertical(VerticalNetwork):
	
	def handles_starting_gradient(self):
		return True
	
	def __init__(self, output_size, hidden_layer_sizes=[], activation_fn=tf.nn.elu, layer_normalization_flag=False):
		self.output_size = output_size
		self.hidden_layer_sizes = hidden_layer_sizes
		self.activation_fn = activation_fn
		self.layer_normalization_flag = layer_normalization_flag

	##
	# @brief For this network, returns layers using the shared parameters.
	def get_vertical_network(
		self, 
		a_input, 
		a_namespace, 
		a_training, 
		a_timestep, 
		reuse
		):
		
		#~ print("a_input", a_input)
		
		layer = a_input
		
		for i in range(len(self.hidden_layer_sizes)):
			layer = self.vertical_layer(
				layer,
				self.hidden_layer_sizes[i],
				activation_fn=self.activation_fn,
				reuse=reuse,
				scope="hidden_layer_" + str(i),
				layer_normalization_flag=self.layer_normalization_flag
				)
		
		output = self.vertical_layer(
				layer,
				self.output_size,
				activation_fn=None,
				reuse=reuse,
				scope="output_layer",
				layer_normalization_flag=self.layer_normalization_flag
				)
		
		return output
	
	def vertical_layer(
		self,
		inputs,
		output_size,
		activation_fn,
		reuse,
		scope,
		layer_normalization_flag
		):
		if layer_normalization_flag:
			res = fc(
				inputs,
				output_size,
				activation_fn=None,
				reuse=reuse,
				scope=scope,
				biases_initializer=None
				)
			res = tf.contrib.layers.layer_norm(
				res,
				activation_fn=activation_fn,
				reuse=reuse,
				scope=scope + "_layer_norm"
				)
		else:
			res = fc(
				inputs,
				output_size,
				activation_fn=activation_fn,
				reuse=reuse,
				scope=scope
				)
		return res


############################################################################################################	
############################################################################################################	
############################################################################################################


##
# @brief Get a fully connected network as in Jentzen et al but share all the learnt
# parameters (eg beta, gamma, W) between the timesteps (but not the moving means). 
#
# This is equivalent to assuming the network should learn an identical response in all 
# timesteps, i.e. that Z is function only of (mu_t, sigma_t) (batch moving means and vars). 
#
class SingleWeights_Vertical_ConcatTime(SingleWeights_Vertical):
	
	##
	# @brief For this network, returns layers using the shared parameters.
	#
	def get_vertical_network(
		self, 
		a_input, 
		a_namespace, 
		a_training,
		a_timestep, 
		reuse
		):
		
		sample_size = tf.shape(a_input)[0]
		tstep = tf.cast(tf.tile(tf.constant([[a_timestep]]), [sample_size, 1]), dtype=tf.float32)
		net_input = tf.concat([a_input, tstep], axis=1)
		
		output = super().get_vertical_network(
				a_input=net_input,
				a_namespace=a_namespace,
				a_training=a_training,
				a_timestep=a_timestep,
				reuse=reuse
			)
		
		return output



############################################################################################################	
############################################################################################################	
############################################################################################################



class SingleWeights_Vertical_Shortcut(SingleWeights_Vertical):
	
	##
	# @brief For this network, returns layers using the shared parameters.
	#
	def get_vertical_network(
		self, 
		a_input, 
		a_namespace, 
		a_training,
		a_timestep, 
		reuse
		):
		
		sample_size = tf.shape(a_input)[0]
		tstep = tf.cast(tf.tile(tf.constant([[a_timestep]]), [sample_size, 1]), dtype=tf.float32)
		net_input = tf.concat([a_input, tstep], axis=1)
		
		if len(self.hidden_layer_sizes) > 0:
			layer = self.vertical_layer(
				net_input,
				self.hidden_layer_sizes[0],
				activation_fn=self.activation_fn,
				reuse=reuse,
				scope="hidden_layer_0",
				layer_normalization_flag=self.layer_normalization_flag
				)
		else:
			return self.vertical_layer(
				tf.concat(net_input, axis=1),
				self.output_size,
				activation_fn=None,
				reuse=reuse,
				scope="output_layer",
				layer_normalization_flag=self.layer_normalization_flag
				)
		
		for i in range(1, len(self.hidden_layer_sizes)):
			layer = self.vertical_layer(
				tf.concat([layer, net_input], axis=1),
				self.hidden_layer_sizes[i],
				activation_fn=self.activation_fn,
				reuse=reuse,
				scope="hidden_layer_" + str(i),
				layer_normalization_flag=self.layer_normalization_flag
				)
		
		output = self.vertical_layer(
				tf.concat([layer, net_input], axis=1),
				self.output_size,
				activation_fn=None,
				reuse=reuse,
				scope="output_layer",
				layer_normalization_flag=self.layer_normalization_flag
				)
				
		return output



############################################################################################################	
############################################################################################################	
############################################################################################################



class SingleWeights_Vertical_Residual(SingleWeights_Vertical):
	
	##
	# @brief For this network, returns layers using the shared parameters.
	#
	def get_vertical_network(
		self, 
		a_input, 
		a_namespace, 
		a_training,
		a_timestep, 
		reuse
		):
		
		sample_size = tf.shape(a_input)[0]
		tstep = tf.cast(tf.tile(tf.constant([[a_timestep]]), [sample_size, 1]), dtype=tf.float32)
		net_input = tf.concat([a_input, tstep], axis=1)
		
		layer = net_input
		
		for i in range(0, len(self.hidden_layer_sizes)):
			layer = self.vertical_layer(
				layer,
				self.hidden_layer_sizes[i],
				activation_fn=self.activation_fn,
				reuse=reuse,
				scope="hidden_layer_" + str(i),
				layer_normalization_flag=self.layer_normalization_flag
				)
			
			if i == 0:
				residual_layer = layer
			elif i % 2 == 0 or i == len(self.hidden_layer_sizes)-1:
				if residual_layer.get_shape().as_list()[1] != layer.get_shape().as_list()[1]:
					# Make a projection layer
					residual_layer = fc(
						residual_layer,
						self.hidden_layer_sizes[i],
						activation_fn=None,
						reuse=reuse,
						scope="projection_layer_" + str(i),
						biases_initializer=None
						)
				layer += residual_layer
				residual_layer = layer
		
		output = self.vertical_layer(
				layer,
				self.output_size,
				activation_fn=None,
				reuse=reuse,
				scope="output_layer",
				layer_normalization_flag=self.layer_normalization_flag
				)
				
		return output
