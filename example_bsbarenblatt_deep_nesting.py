##
# Example use of dnesting on a Black-Scholes-Barenblatt equation
# From https://arxiv.org/abs/1809.07609
#

import numpy as np
import tensorflow as tf
import deep_nesting as dnesting
import time
import os
import matplotlib.pyplot as plt


####
# In this example, we solve a Black-Scholes-Barenblatt equation using our new algorithm.
# Use example: 
# python example_bsbarenblatt_deep_nesting.py
# Warning: this uses a lot of memory. Decrease sp.n_inner if full
####

##
# Parameters

monte_carlo_size = 1024
min_decrease_rate = 0.05

d=100
case_name = "BlackScholesBarenblatt"

T = 1.
X0 = np.array([1.0,0.5]*int(d/2), dtype=np.float32)
n_timesteps=100

bse = dnesting.BlackScholesBarenblatt(
        sigma_bar=0.4,
        r=0.05,
        T=T
        )

print("----- Solving BlackScholesBarenblatt -----")

sp = dnesting.XavierSolver.Parameters(
        bse,
        d=d,
        T=T,
        n_timesteps=n_timesteps,
        X0=X0,
        Yini=[.7, 1.3]
        )

sp.valid_size = monte_carlo_size

sp.learning_rate = 0.01
# Change the min_iter and max_iter to a smaller value to stop the algorithm earlier
sp.n_miniter = 10000#2000
sp.n_maxiter = 10000#2000

sp.batch_size = 300
sp.valid_size = 300
sp.normalize_input_X = True

sp.num_hidden_layers = 3
sp.hidden_layer_size = 2*d
sp.activation_fn = tf.nn.tanh

sp.n_inner = 4000
sp.lamb = 1.0
sp.t_distrib_a = 0.0 # This changes the distribution of the tau to be asymetric. Zero is flat


solver = dnesting.XavierSolver(sp, base_name=case_name, output_directory="./output/test_deep_xavier_bsbarenblattd100/")

print("now training")
solver.train(run_name="Test", min_decrease_rate=min_decrease_rate)
print("end")