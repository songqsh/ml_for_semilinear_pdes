##
# Example use of DBSDE on an example with square root terminal condition
# From https://arxiv.org/abs/1809.07609
#

import numpy as np
import tensorflow as tf
import deep_bsde as dbsde
import time
import os



####


ntsteps = 100 # MC solutions are provided for N=20, 40, 60, 80, 100, 150, 200
print("ntsteps", ntsteps)
hidden_layer_size = 30
print("hidden_layer_size", hidden_layer_size)
number_hidden_layers = 2
print("number_hidden_layers", number_hidden_layers)

run_index = 1
output_dir_name = "./output/richou_d10_deep_bsde/"

network_type = 5
print("network_type", network_type)

### Network types:
# 0: FC DBSDE
# 1: FC ELU
# 2: FC Residual
# 3: FC Merged
# 4: FC Merged Shortcut
# 5: FC Merged Residual
# 6: LSTM
# 7: Augmented LSTM
# 8: Hybrid LSTM
# 9: Residual LSTM

########################################################################
# PARAMETERS
########################################################################


##### DIMENSION

d=10

##### TRAINING

batch_size=300
monte_carlo_size=1000

min_decrease_rate=0.05
min_iter=16000
max_iter=16000

learning_rate=0.01

normalize_input_X=True

##### PDE and SOLVER

T = 1.
X0 = np.zeros((d,), dtype=np.float32)
Yini = [0., 1.]
n_timesteps=ntsteps


########################################################################
# PDE
########################################################################


alpha=0.5
pde = dbsde.RichouToyExample(
	a_alpha=alpha,
	a_T = T
)
case_name = "RichouToyExample_d_" + str(d) + "_alpha_" + str(alpha) + "_T_" + str(T)


########################################################################
# RESOLUTION
########################################################################


sp = dbsde.SimpleSolver.Parameters(
	pde,
	d=d,
	T=T,
	n_timesteps=n_timesteps,
	X0=X0,
	Yini=Yini
	)
sp.learning_rate = learning_rate
sp.valid_size = monte_carlo_size
sp.batch_size = batch_size
sp.n_miniter = min_iter
sp.n_maxiter = max_iter
sp.normalize_input_X = normalize_input_X


### Computing the MC solution with a lot of threads (server)
### or fetch precomputed MC solution (see README)
sp.override_mc=True
sp.override_mc_size=50000
sp.override_n_threads=16


########################################################################
# NETWORK DEFINITION
########################################################################

### Network types:
# 0: FC DBSDE
# 1: FC ELU
# 2: FC Residual
# 3: FC Merged
# 4: FC Merged Shortcut
# 5: FC Merged Residual
# 6: LSTM
# 7: Augmented LSTM
# 8: Hybrid LSTM
# 9: Residual LSTM

def get_output_file_name(network_name):
	s = output_dir_name + network_name + "_" + str(ntsteps) + "_ntsteps_" + str(run_index) + "/"
	return s


if network_type == 0:

	sp.network = dbsde.SimpleNetwork(output_size=d, hidden_layer_sizes=[hidden_layer_size]*number_hidden_layers, activation_fn=tf.nn.relu, batch_normalization_flag=True)
	output_dir_name_complete = get_output_file_name("FC_DBSDE_ReLU_BN")
	solver = dbsde.SimpleSolver(sp, seed=1, base_name=case_name, output_directory=output_dir_name_complete)

elif network_type == 1:
	
	sp.network = dbsde.SimpleNetwork(output_size=d, hidden_layer_sizes=[hidden_layer_size]*number_hidden_layers, activation_fn=tf.nn.elu, batch_normalization_flag=False)
	output_dir_name_complete = get_output_file_name("FC_DBSDE_ELU")
	solver = dbsde.SimpleSolver(sp, seed=1, base_name=case_name, output_directory=output_dir_name_complete)

elif network_type == 2:
	
	sp.network = dbsde.SimpleNetwork_Vertical_Residual(output_size=d, hidden_layer_sizes=[hidden_layer_size]*number_hidden_layers, activation_fn=tf.nn.elu, layer_normalization_flag=False)
	output_dir_name_complete = get_output_file_name("FC_DBSDE_Residual_ELU")
	solver = dbsde.SimpleSolver_gX(sp, seed=1, base_name=case_name, output_directory=output_dir_name_complete)


elif network_type == 3:
	
	sp.network = dbsde.SingleWeights_Vertical_ConcatTime(output_size=d, hidden_layer_sizes=[hidden_layer_size]*number_hidden_layers, activation_fn=tf.nn.elu, layer_normalization_flag=False)
	output_dir_name_complete = get_output_file_name("Shared_FC_DBSDE_ELU")
	solver = dbsde.SimpleSolver_Recurrent_Y_gX(sp, seed=1, base_name=case_name, output_directory=output_dir_name_complete)

elif network_type == 4:

	sp.network = dbsde.SingleWeights_Vertical_Shortcut(output_size=d, hidden_layer_sizes=[hidden_layer_size]*number_hidden_layers, activation_fn=tf.nn.elu, layer_normalization_flag=False)
	output_dir_name_complete = get_output_file_name("Shared_FC_Shortcut_DBSDE_ELU")
	solver = dbsde.SimpleSolver_Recurrent_Y_gX(sp, seed=1, base_name=case_name, output_directory=output_dir_name_complete)

elif network_type == 5:
	
	sp.network = dbsde.SingleWeights_Vertical_Residual(output_size=d, hidden_layer_sizes=[hidden_layer_size]*number_hidden_layers, activation_fn=tf.nn.elu, layer_normalization_flag=False)
	output_dir_name_complete = get_output_file_name("Shared_FC_Residual_DBSDE_ELU")
	solver = dbsde.SimpleSolver_Recurrent_Y_gX(sp, seed=1, base_name=case_name, output_directory=output_dir_name_complete)

elif network_type == 6:

	sp.network = dbsde.LSTMBlockFusedCell_ConcatTime(output_size=d, state_size=hidden_layer_size, num_layers=number_hidden_layers)
	output_dir_name_complete = get_output_file_name("LSTMBlockFusedCell_ConcatTime")
	solver = dbsde.SimpleSolver(sp, seed=1, base_name=case_name, output_directory=output_dir_name_complete)

elif network_type == 7:
	
	output_dir_name_complete = get_output_file_name("LSTM_DBSDE_Deep")
	solver = dbsde.Solver_RecurrentLSTM_deep(sp, num_layers=number_hidden_layers, hidden_layer_width=hidden_layer_size, activation=tf.nn.tanh, seed=1, base_name=case_name, output_directory=output_dir_name_complete)

elif network_type == 8:

	output_dir_name_complete = get_output_file_name("LSTM_DBSDE_Deep_hybrid")
	solver = dbsde.Solver_RecurrentLSTM_deep_hybrid(sp, num_layers=number_hidden_layers, hidden_layer_width=hidden_layer_size, activation=tf.nn.tanh, seed=1, base_name=case_name, output_directory=output_dir_name_complete)
    
elif network_type == 9:

	output_dir_name_complete = get_output_file_name("LSTM_DBSDE_Deep_Residual")
	solver = dbsde.Solver_RecurrentLSTM_deep_residual(sp, num_layers=number_hidden_layers, hidden_layer_width=hidden_layer_size, activation=tf.nn.tanh, seed=1, base_name=case_name, output_directory=output_dir_name_complete)


solver.train(run_name=case_name, min_decrease_rate=min_decrease_rate)
