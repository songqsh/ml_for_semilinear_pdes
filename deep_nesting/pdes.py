##
# Implements PDEs for DBSDE solvers
# From https://arxiv.org/abs/1809.07609
#

import abc

import numpy as np
import tensorflow as tf
import scipy.integrate as integrate

import multiprocessing
from joblib import Parallel, delayed

import hashlib

_MULTIPROCESSING_CORE_COUNT = 2
_NUM_MC_RANDOM_SAMPLES = 10000

##
# @brief Abstract base class for a semilinear PDE.
#
# The semilinear PDE should be defined by
# u_t + mu * grad(u) + 1/2 * Tr(sigma * sigma^T * Hess(u)) + f = 0
# with final condition u(T, x) = g(x), where diff_np is mu, vol_np is
# sigma, f_tf is f, g_tf is g.
class SemilinearPDE(abc.ABC):
	
	@abc.abstractmethod
	##
	# @brief Defines the diffusion function for the PDE (NumPy format) -
	# prefer overriding this function with get_next in case an explicit
	# solution is known.
	#
	# @param self
	# @param t Current time.
	# @param X Current state.
	# @param delta_t Time interval to the next state.
	#
	# @return The diffusion function: diff * delta_t.
	def diff_np(self, t, X, delta_t):
		pass
	
	@abc.abstractmethod
	##
	# @brief Defines the volatility function for the PDE (NumPy format) -
	# prefer overriding this function with get_next in case an explicit
	# solution is known.
	#
	# @param self
	# @param t Current time.
	# @param X Current state.
	# @param delta_t Brownian motion to the next state.
	#
	# @return The diffusion function: vol.dot(delta_b).
	def vol_np(self, t, X, delta_b):
		pass
	
	@abc.abstractmethod
	##
	# @brief Defines the volatility function for the PDE (TensorFlow format) -
	# prefer overriding this function with get_next in case an explicit
	# solution is known.
	#
	# @param self
	# @param t Current time.
	# @param X Current state.
	# @param delta_t Brownian motion to the next state.
	#
	# @return The diffusion function: vol.dot(delta_b).
	def vol_tf(self, t, X, delta_b):
		pass
	
	##
	#
	# @param self
	# @param t Current time.
	# @param X Current state.
	# @param delta_t Brownian motion to the next state.
	#
	# @return sigma^-T
	@abc.abstractmethod
	def vol_tf_mT(self, t, X, delta_b):
		pass
	
	@abc.abstractmethod
	##
	# @brief Defines the nonlinear function (tf.Tensor format).
	#
	# @param self
	# @param t Current time.
	# @param X Current state.
	# @param Y Current Y.
	# @param Z Current Z.
	#
	# @return The nonlinear function's tensor.
	def f_tf(self, t, X, Y, Z):
		pass
		
	@abc.abstractmethod
	##
	# @brief Defines the final condition function (tf.Tensor format).
	#
	# @param self
	# @param X Current state.
	#
	# @return The final condition function's tensor.
	def g_tf(self, X):
		pass
	
	@abc.abstractmethod
	##
	# @brief Defines the final condition function (NumPy array format).
	#
	# @param self
	# @param X Current state.
	#
	# @return The final condition function's tensor.
	def g_np(self, X):
		pass
	
	##
	# @brief Compute X_{t+1} from arguments (in NumPy format).
	# 
	# @param self
	# @param t Time
	# @param X X_t
	# @param delta_t {t+1} - t
	# @param delta_b B_{t+1}-B_t
	#
	# @return X_{t+1}
	def get_next(self, t, X, delta_t, delta_b):
		return X + self.diff_np(t, X, delta_t) + self.vol_np(t, X, delta_b)
		
	def get_next_tf(self, t, X, delta_t, delta_b):
		return X + self.diff_tf(t, X, delta_t) + self.vol_tf(t, X, delta_b)
	
	##
	# @brief Generate paths from initial conditions with provided integration parameters.
	# 
	# @param self
	# @param X0 Initial condition.
	# @param delta_t {t+1} - t
	# @param n_path_count Number of paths to be generated.
	# @param n_step Number of integration steps.
	# @param t0 Initial time.
	#
	# @return X Sample paths.
	# @return dB Sample Brownian motion.
	def generate_paths(self, X0, delta_t, n_path_count, n_step, t0=0, seed=None):
		n_dim = X0.size
		X = np.zeros((n_path_count, n_step+1, n_dim), dtype=np.float32)
		X[:, 0, :] = X0
		
		# Generate dB
		if seed is not None:
			np.random.seed(seed)
		
		# Transpose for retrocompatibility with mc solutions generated with deep_bsde
		# in which the data format is (n_path_count, n_dim, n_step)
		dB = np.transpose(np.random.normal(loc=0.0, scale=np.sqrt(delta_t), size=(n_path_count, n_dim, n_step)).astype(np.float32), (0, 2, 1)) # Variance = delta_t

		for i in range(0, n_step):
			# Propagation using Euler
			X[:, i+1, :] = self.get_next(t0 + i * delta_t, X[:, i, :], delta_t, dB[:, i, :])
		
		return X, dB
		

############################################################################################################	
############################################################################################################	
############################################################################################################	


##
# @brief Semilinear PDE with a known reference solution
class SemilinearPDE_ReferenceSolution(SemilinearPDE):
	
	@abc.abstractmethod
	def reference_solution(self, t, X):
		pass
	
	@abc.abstractmethod
	def reference_gradient(self, t, X):
		pass
	
	@abc.abstractmethod
	def reference_gradient_tf(self, t, X):
		pass


##
# @brief Semilinear PDE with a known reference solution
class SemilinearPDE_MCSolution(SemilinearPDE):
	
	@abc.abstractmethod
	def get_solution_from_file(self, d, n_timesteps, X):
		pass


############################################################################################################	
############################################################################################################	
############################################################################################################	


##
# @brief Implements HJB equation.
# 
# u_t + laplacian(u)_x + u - u^3 = 0
# mu = [0, ..., 0]^T
# sigma * b = sqrt(2) * b
# f(..., Y, ...) = - lambda * norm(grad(u))^2
# g(..., X) = log(0.5 * (1 + norm(x)^2))
class HamiltonJacobiBellman(SemilinearPDE_MCSolution):
	
	def __init__(self, a_lambd, a_T):
		self.lambd = a_lambd
		self.T = a_T
	
	def diff_np(self, t, X, delta_t):
		return 0. #np.zeros(X.shape, X.dtype)
	
	def vol_np(self, t, X, b):
		return np.sqrt(2) * b
	
	def vol_tf(self, t, X, b):
		return np.sqrt(2.) * b
	
	def vol_tf_mT(self, t, X, b):
		return b / np.sqrt(2.)
		
	def vol_np_mT(self, t, X, b):
		return b / np.sqrt(2.)
	
	def get_next(self, t, X, delta_t, delta_b):
		return X + np.sqrt(2.) * delta_b
		
	def get_next_tf(self, t, X, delta_t, delta_b):
		return X + np.sqrt(2.) * delta_b
		
	def f_tf(self, t, X, Y, Z):
		return - self.lambd * tf.reduce_sum(tf.pow(self.vol_tf_mT(t, X, Z), 2), -1, keep_dims=True) # Have to divide by the volatility squared since Z = sigma^T grad u
		
	def f_np(self, t, X, Y, Z):
		return - self.lambd * np.sum(np.power(self.vol_np_mT(t, X, Z), 2), -1, keepdims=True) # Have to divide by the volatility squared since Z = sigma^T grad u
		
	def g_tf(self, X):
		return tf.log(0.5 * (1 + tf.reduce_sum(tf.pow(X, 2), -1, keep_dims=True)))
	
	def g_np(self, X):
		return np.log(0.5 * (1 + np.sum(np.power(X, 2), -1, keepdims=True)))
	
	def g_p_np(self, X):
		return 2 * X /(1 + np.sum(np.power(X, 2), -1, keepdims=True))
	
	def get_solution_from_file(self, d, n_timesteps, X, override_mc_size=None, override_n_threads=1):
		# Considering a set of parameters, try to read a solution file
		# If nonexisting, compute the solution and save it
		
		# s is a combination of the parameters
		s = str(self.lambd) + str(self.T) + str(d) + str(n_timesteps)
		
		print("s", s)
		m = hashlib.md5()
		m.update(s.encode('utf-8'))
		hashstr = str(m.hexdigest())
		
		print("hashstr", hashstr)
		
		if override_mc_size is None:
			Y_filename = "./mc_solutions/" + "HJB_Y_" + hashstr
			Z_filename = "./mc_solutions/" + "HJB_Z_" + hashstr
		else:
			Y_filename = "./mc_solutions/" + "HJB_Y_" + hashstr + "_" + str(override_mc_size)
			Z_filename = "./mc_solutions/" + "HJB_Z_" + hashstr + "_" + str(override_mc_size)
		
		nb_realizations = X.shape[0]
		
		# Try to read the file
		Ysol = np.zeros((nb_realizations, n_timesteps+1, 1), dtype=np.float32)
		Zsol = np.zeros((nb_realizations, n_timesteps, d), dtype=np.float32)
		
		try:
			Ysol[:, :, 0] = np.loadtxt(Y_filename, dtype=np.float32)
			for i in range(d):
				Zsol[:, :, i] = np.loadtxt(Z_filename + "_" + str(i), dtype=np.float32)
		except:
			
			print("No available MC solution")
			
		return Ysol, Zsol


############################################################################################################	
############################################################################################################	
############################################################################################################




##
# @brief Implements Black-Scholes equation with default risk.
# 
# See 3.1 in Han, Jentzen, E
class BlackScholesDefaultRisk(SemilinearPDE):
		
	def __init__(self, delta, R, mu_bar, sigma_bar, v_h, v_l, gamma_h, gamma_l):
		self.delta = np.array(delta, np.float32)
		self.R = np.array(R, np.float32)
		self.mu_bar = np.array(mu_bar, np.float32)
		self.sigma_bar = np.array(sigma_bar, np.float32)
		self.v_h = np.array(v_h, np.float32)
		self.v_l = np.array(v_l, np.float32)
		self.gamma_h = np.array(gamma_h, np.float32)
		self.gamma_l = np.array(gamma_l, np.float32)
	
	def diff_np(self, t, X, delta_t):
		#~ return 0
		#~ return self.mu_bar * X * delta_t
		return self.mu_bar * X * delta_t
		# Overriden by get_next
		#~ pass
	
	def vol_np(self, t, X, b):
		#~ return np.multiply(self.sigma_bar * X, b)
		return np.multiply(self.sigma_bar * X, b)
		# Overriden by get_next
		#~ pass
	
	def vol_tf(self, t, X, b):
		#~ return tf.multiply(self.sigma_bar * X, b)
		return tf.multiply(self.sigma_bar * X, b)
		
	def vol_tf_mT(self, t, X, b):
		#~ return tf.divide(b, self.sigma_bar * X)
		return tf.divide(b, self.sigma_bar * X)
		
	def vol_np_mT(self, t, X, b):
		#~ return tf.divide(b, self.sigma_bar * X)
		return b / (self.sigma_bar * X)
	
	def get_next(self, t, X, delta_t, delta_b):
		return X * np.exp((self.mu_bar - self.sigma_bar**2/2.) * delta_t + self.sigma_bar * delta_b)
		
	def get_next_tf(self, t, X, delta_t, delta_b):
		return X * tf.exp((self.mu_bar - self.sigma_bar**2/2.) * delta_t + self.sigma_bar * delta_b)
	
	def f_tf(self, t, X, Y):
		return -(1-self.delta) * tf.minimum(self.gamma_h, tf.maximum(self.gamma_l, ((self.gamma_h-self.gamma_l)/(self.v_h-self.v_l)) * (Y - self.v_h) + self.gamma_h)) * Y - self.R * Y#-(1-self.delta) * (((self.gamma_h-self.gamma_l)/(self.v_h-self.v_l)) * (Y - self.v_h) + self.gamma_h) * Y - self.R * Y#
	
	def f_np(self, t, X, Y):
		return -(1-self.delta) * np.minimum(self.gamma_h, np.maximum(self.gamma_l, ((self.gamma_h-self.gamma_l)/(self.v_h-self.v_l)) * (Y - self.v_h) + self.gamma_h)) * Y - self.R * Y#-(1-self.delta) * (((self.gamma_h-self.gamma_l)/(self.v_h-self.v_l)) * (Y - self.v_h) + self.gamma_h) * Y - self.R * Y#

	def g_tf(self, X):
		return tf.reduce_min(X, -1, keep_dims=True)#tf.mean(X, 1, keep_dims=True) #
	
	def g_np(self, X):
		return np.amin(X, axis=-1, keepdims=True)#np.mean(X, 1, keepdims=True) #


############################################################################################################	
############################################################################################################	
############################################################################################################




##
# @brief Implements Black-Scholes equation with default risk.
# 
# See 3.1 in Han, Jentzen, E
class BlackScholesDefaultRisk_softmin(SemilinearPDE):
		
	def __init__(self, delta, R, mu_bar, sigma_bar, v_h, v_l, gamma_h, gamma_l):
		self.delta = np.array(delta, np.float32)
		self.R = np.array(R, np.float32)
		self.mu_bar = np.array(mu_bar, np.float32)
		self.sigma_bar = np.array(sigma_bar, np.float32)
		self.v_h = np.array(v_h, np.float32)
		self.v_l = np.array(v_l, np.float32)
		self.gamma_h = np.array(gamma_h, np.float32)
		self.gamma_l = np.array(gamma_l, np.float32)
	
	def diff_np(self, t, X, delta_t):
		#~ return 0
		#~ return self.mu_bar * X * delta_t
		return self.mu_bar * X * delta_t
		# Overriden by get_next
		#~ pass
	
	def vol_np(self, t, X, b):
		#~ return np.multiply(self.sigma_bar * X, b)
		return np.multiply(self.sigma_bar * X, b)
		# Overriden by get_next
		#~ pass
	
	def vol_tf(self, t, X, b):
		#~ return tf.multiply(self.sigma_bar * X, b)
		return tf.multiply(self.sigma_bar * X, b)
		
	def vol_tf_mT(self, t, X, b):
		#~ return tf.divide(b, self.sigma_bar * X)
		return tf.divide(b, self.sigma_bar * X)
		
	def vol_np_mT(self, t, X, b):
		#~ return tf.divide(b, self.sigma_bar * X)
		return b / (self.sigma_bar * X)
	
	def get_next(self, t, X, delta_t, delta_b):
		return X * np.exp((self.mu_bar - self.sigma_bar**2/2.) * delta_t + self.sigma_bar * delta_b)
		
	def get_next_tf(self, t, X, delta_t, delta_b):
		return X * tf.exp((self.mu_bar - self.sigma_bar**2/2.) * delta_t + self.sigma_bar * delta_b)
	
	def f_tf(self, t, X, Y):
		return -(1-self.delta) * tf.minimum(self.gamma_h, tf.maximum(self.gamma_l, ((self.gamma_h-self.gamma_l)/(self.v_h-self.v_l)) * (Y - self.v_h) + self.gamma_h)) * Y - self.R * Y#-(1-self.delta) * (((self.gamma_h-self.gamma_l)/(self.v_h-self.v_l)) * (Y - self.v_h) + self.gamma_h) * Y - self.R * Y#
	
	def f_np(self, t, X, Y):
		return -(1-self.delta) * np.minimum(self.gamma_h, np.maximum(self.gamma_l, ((self.gamma_h-self.gamma_l)/(self.v_h-self.v_l)) * (Y - self.v_h) + self.gamma_h)) * Y - self.R * Y#-(1-self.delta) * (((self.gamma_h-self.gamma_l)/(self.v_h-self.v_l)) * (Y - self.v_h) + self.gamma_h) * Y - self.R * Y#

	def g_tf(self, X):
		alpha = -0.1
		expalphaX = tf.exp(alpha * X)
		num = tf.reduce_sum(X * expalphaX, -1, keep_dims=True)
		den = tf.reduce_sum(expalphaX, -1, keep_dims=True)
		return num/den
		#tf.reduce_min(X, -1, keep_dims=True)#tf.mean(X, 1, keep_dims=True) #
	
	def g_np(self, X):
		alpha = -0.1
		expalphaX = np.exp(alpha * X)
		num = np.sum(X * expalphaX, axis=-1, keepdims=True)
		den = np.sum(expalphaX, axis=-1, keepdims=True)
		return num/den
		#np.amin(X, axis=-1, keepdims=True)#np.mean(X, 1, keepdims=True) #


############################################################################################################	
############################################################################################################	
############################################################################################################




##
# @brief Implements Black-Scholes equation with default risk.
# 
# See 3.1 in Han, Jentzen, E
class BlackScholesDefaultRisk_exp(SemilinearPDE):
		
	def __init__(self, delta, R, mu_bar, sigma_bar, v_h, v_l, gamma_h, gamma_l):
		self.delta = np.array(delta, np.float32)
		self.R = np.array(R, np.float32)
		self.mu_bar = np.array(mu_bar, np.float32)
		self.sigma_bar = np.array(sigma_bar, np.float32)
		self.v_h = np.array(v_h, np.float32)
		self.v_l = np.array(v_l, np.float32)
		self.gamma_h = np.array(gamma_h, np.float32)
		self.gamma_l = np.array(gamma_l, np.float32)
	
	def diff_np(self, t, X, delta_t):
		#~ return 0
		#~ return self.mu_bar * X * delta_t
		return (self.mu_bar - self.sigma_bar**2/2.) * delta_t
		# Overriden by get_next
		#~ pass
	
	def vol_np(self, t, X, b):
		#~ return np.multiply(self.sigma_bar * X, b)
		return np.multiply(self.sigma_bar, b)
		# Overriden by get_next
		#~ pass
	
	def vol_tf(self, t, X, b):
		#~ return tf.multiply(self.sigma_bar * X, b)
		return tf.multiply(self.sigma_bar, b)
		
	def vol_tf_mT(self, t, X, b):
		#~ return tf.divide(b, self.sigma_bar * X)
		return tf.divide(b, self.sigma_bar)
		
	def vol_np_mT(self, t, X, b):
		#~ return tf.divide(b, self.sigma_bar * X)
		return b / self.sigma_bar
	
	def get_next(self, t, X, delta_t, delta_b):
		return X + (self.mu_bar - self.sigma_bar**2/2.) * delta_t + self.sigma_bar * delta_b
		
	def get_next_tf(self, t, X, delta_t, delta_b):
		return X + (self.mu_bar - self.sigma_bar**2/2.) * delta_t + self.sigma_bar * delta_b
	
	def f_tf(self, t, X, Y):
		return -(1-self.delta) * tf.minimum(self.gamma_h, tf.maximum(self.gamma_l, ((self.gamma_h-self.gamma_l)/(self.v_h-self.v_l)) * (Y - self.v_h) + self.gamma_h)) * Y - self.R * Y#-(1-self.delta) * (((self.gamma_h-self.gamma_l)/(self.v_h-self.v_l)) * (Y - self.v_h) + self.gamma_h) * Y - self.R * Y#
	
	def f_np(self, t, X, Y):
		return -(1-self.delta) * np.minimum(self.gamma_h, np.maximum(self.gamma_l, ((self.gamma_h-self.gamma_l)/(self.v_h-self.v_l)) * (Y - self.v_h) + self.gamma_h)) * Y - self.R * Y#-(1-self.delta) * (((self.gamma_h-self.gamma_l)/(self.v_h-self.v_l)) * (Y - self.v_h) + self.gamma_h) * Y - self.R * Y#

	def g_tf(self, X):
		return tf.reduce_min(tf.exp(X), -1, keep_dims=True)#tf.mean(X, 1, keep_dims=True) #
	
	def g_np(self, X):
		return np.amin(np.exp(X), axis=-1, keepdims=True)#np.mean(X, 1, keepdims=True) #


############################################################################################################	
############################################################################################################	
############################################################################################################





class BlackScholesBarenblatt(SemilinearPDE_ReferenceSolution):
		
	def __init__(self, sigma_bar, r, T):
		self.sigma_bar = sigma_bar
		self.r = r
		self.T = T
	
	def diff_np(self, t, X, delta_t):
		return 0
	
	def vol_np(self, t, X, b):
		return np.multiply(self.sigma_bar * X, b)
	
	def vol_tf(self, t, X, b):
		return tf.multiply(self.sigma_bar * X, b)
	
	def vol_tf_mT(self, t, X, delta_b):
		return tf.divide(delta_b, self.sigma_bar * X)
		
	def vol_np_mT(self, t, X, delta_b):
		return delta_b / (self.sigma_bar * X)
	
	def get_next(self, t, X, delta_t, delta_b):
		return np.multiply(np.exp(self.sigma_bar * delta_b - (self.sigma_bar**2/2.) * delta_t), X)
		
	def get_next_tf(self, t, X, delta_t, delta_b):
		return tf.multiply(tf.exp(self.sigma_bar * delta_b - (self.sigma_bar**2/2.) * delta_t), X)
	
	def f_tf(self, t, X, Y, Z):
		return - self.r * (Y - tf.reduce_sum(Z, axis=-1, keep_dims=True) / self.sigma_bar)

	def f_tf_grad(self, t, X, Y, grad):
		return - self.r * (Y - tf.reduce_sum(tf.multiply(grad, X), axis=-1, keep_dims=True))
		
	def f_np(self, t, X, Y, Z):
		return - self.r * (Y - np.sum(Z, axis=-1, keepdims=True) / self.sigma_bar)

	def f_np_grad(self, t, X, Y, grad):
		return - self.r * (Y - np.sum(np.multiply(grad, X), axis=-1, keepdims=True))

	def g_tf(self, X):
		return tf.reduce_sum(tf.pow(X, 2), -1, keep_dims=True)
	
	def g_np(self, X):
		return np.sum(np.power(X, 2), -1, keepdims=True) 

	def g_p_np(self, X):
		return 2.0 * X
		
	def g_p_tf(self, X):
		return 2.0 * X

	def reference_solution(self, t, X):
		return np.exp((self.r + self.sigma_bar**2) * (self.T - t)) * self.g_np(X)
	
	def reference_gradient(self, t, X):
		return np.exp((self.r + self.sigma_bar**2) * (self.T - t)) * self.g_p_np(X)

	def reference_gradient_tf(self, t, X):
		return tf.cast(tf.exp((self.r + self.sigma_bar**2) * (self.T - t)), tf.float32) * self.g_p_tf(X)


############################################################################################################	
############################################################################################################	
############################################################################################################	


## 
# @brief Implements Black-Scholes equation with different interest rates for borrowing and lending.
# 
# See 4.4 in E, Han and Jentzen
class BlackScholesInterestRates(SemilinearPDE):
		
	def __init__(self, mu_bar, sigma_bar, R_l, R_b):
		self.mu_bar = np.array(mu_bar, np.float32)
		self.sigma_bar = np.array(sigma_bar, np.float32)
		self.R_l = np.array(R_l, np.float32)
		self.R_b = np.array(R_b, np.float32)
	
	def diff_np(self, t, X, delta_t):
		#~ return self.mu_bar * X * delta_t
		# Overriden by get_next
		pass
	
	def vol_np(self, t, X, b):
		#~ return np.multiply(self.sigma_bar * X, b)
		# Overriden by get_next
		pass
	
	def vol_tf(self, t, X, b):
		return tf.multiply(self.sigma_bar * X, b)
		
	def vol_tf_mT(self, t, X, b):
		return tf.divide(b, self.sigma_bar * X)
	
	def get_next(self, t, X, delta_t, delta_b):
		return np.multiply(np.exp((self.mu_bar - self.sigma_bar**2/2.) * delta_t + self.sigma_bar * delta_b), X)
	
	def get_next_tf(self, t, X, delta_t, delta_b):
		return tf.multiply(tf.exp((self.mu_bar - self.sigma_bar**2/2.) * delta_t + self.sigma_bar * delta_b), X)
	
	def f_tf(self, t, X, Y, Z):
		sum_zi = tf.reduce_sum(Z, -1, keep_dims=True)
		return - self.R_l * Y - ((self.mu_bar - self.R_l)/self.sigma_bar) * sum_zi + (self.R_b - self.R_l) * tf.maximum(np.array(0., np.float32), sum_zi / self.sigma_bar - Y)
	
	def g_tf(self, X):
		max_x = tf.reduce_max(X, -1, keep_dims=True)
		return tf.maximum(max_x - 120., np.array(0., np.float32)) - 2. * tf.maximum(max_x - 150., np.array(0., np.float32))

	def g_np(self, X):
		max_x = np.amax(X, axis=-1, keepdims=True)
		return np.maximum(max_x - 120., 0.) - 2 * np.maximum(max_x - 150., 0.)

############################################################################################################	
############################################################################################################	
############################################################################################################	


class FirstToyExample(SemilinearPDE_ReferenceSolution):
	
	def __init__(self, a_mu_zero, a_sigma_zero, a_d, a_T, a_a, a_r):
		self.mu_zero = a_mu_zero
		self.sigma_zero = a_sigma_zero
		self.d = a_d
		self.T = a_T
		self.a = a_a
		self.r = a_r
	
	def diff_np(self, t, X, delta_t):
		#~ return self.mu_zero * delta_t / self.d
		pass
	
	def vol_np(self, t, X, b):
		#~ return self.sigma_zero * b / np.sqrt(self.d)
		pass
	
	def get_next(self, t, X, delta_t, delta_b):
		return X + self.mu_zero * delta_t / self.d + self.sigma_zero * delta_b / np.sqrt(self.d)
	
	def get_next_tf(self, t, X, delta_t, delta_b):
		return X + self.diff_tf(t, X, delta_t) + self.vol_tf(t, X, delta_b)
	
	def diff_tf(self, t, X, delta_t):
		return self.mu_zero * delta_t / self.d
	
	def vol_tf(self, t, X, b):
		return self.sigma_zero * b / np.sqrt(self.d)
	
	def vol_tf_mT(self, t, X, b):
		return np.sqrt(self.d) * b / self.sigma_zero
	
	def f_tf(self, t, X, Y, Z):
		
		exp_a_T_m_t = tf.exp(self.a * (self.T - t))
		sum_X = tf.reduce_sum(X, -1, keep_dims=True)
		cos_sum_X = tf.cos(sum_X)
		sin_sum_X = tf.sin(sum_X)
		
		tmp1 = cos_sum_X * (self.a + self.sigma_zero**2 / 2.) * exp_a_T_m_t
		tmp2 = sin_sum_X * self.mu_zero * exp_a_T_m_t
		tmp3 = self.r * tf.pow(cos_sum_X, 2) * tf.pow(exp_a_T_m_t, 2)
		tmp4 = self.r * tf.pow(tf.maximum(-exp_a_T_m_t, tf.minimum(Y, exp_a_T_m_t)), 2)
		
		return tmp1 + tmp2 - tmp3 + tmp4
	
	def g_tf(self, X):
		return tf.cos(tf.reduce_sum(X, -1, keep_dims=True))
	
	def g_np(self, X):
		return np.cos(np.sum(X, axis=-1, keepdims=True))
		
	def reference_solution(self, t, X):
		return np.exp(self.a * (self.T - t)) * np.cos(np.sum(X, axis=-1, keepdims=True))
	
	def reference_gradient(self, t, X):
		d = X.shape[-1]
		Xres = np.reshape(X, (-1, d))
		tmp = np.tile(-np.exp(self.a * (self.T - t)) * np.sin(np.sum(Xres, axis=-1, keepdims=True)), (1, d))
		return np.reshape(tmp, X.shape)

	def reference_gradient_tf(self, t, X):
		d = tf.shape(X)[-1]
		Xres = tf.reshape(X, [-1, d])
		tmp = tf.tile(-tf.cast(tf.exp(self.a * (self.T - t)), tf.float32) * tf.sin(tf.reduce_sum(Xres, axis=-1, keep_dims=True)), [1, d])
		return tf.reshape(tmp, tf.shape(X))

############################################################################################################	
############################################################################################################	
############################################################################################################	


class SecondToyExample(SemilinearPDE_ReferenceSolution):
	
	def __init__(self, a_mu_zero, a_sigma_zero, a_d, a_T, a_a, a_r):
		self.mu_zero = a_mu_zero
		self.sigma_zero = a_sigma_zero
		self.d = a_d
		self.T = a_T
		self.a = a_a
		self.r = a_r
		
	
	def diff_np(self, t, X, delta_t):
		return self.mu_zero * delta_t / self.d
		#~ pass
	
	def vol_np(self, t, X, b):
		return self.sigma_zero * b / np.sqrt(self.d)
		#~ pass
	
	def get_next(self, t, X, delta_t, delta_b):
		return X + self.mu_zero * delta_t / self.d + self.sigma_zero * delta_b / np.sqrt(self.d)
	
	def get_next_tf(self, t, X, delta_t, delta_b):
		return X + self.mu_zero * delta_t / self.d + self.sigma_zero * delta_b / np.sqrt(self.d)
	
	def diff_tf(self, t, X, delta_t):
		return self.mu_zero * delta_t / self.d
	
	def vol_tf(self, t, X, b):
		return self.sigma_zero * b / np.sqrt(self.d)
		
	def vol_np_mT(self, t, X, b):
		return np.sqrt(self.d) * b / self.sigma_zero
		
	def vol_tf_mT(self, t, X, b):
		return np.sqrt(self.d) * b / self.sigma_zero
	
	def f_tf(self, t, X, Y, Z):
		
		#~ exp_a_T_m_t = tf.constant(np.exp(self.a * (self.T - t)), dtype=tf.float32)
		exp_a_T_m_t = tf.exp(self.a * (self.T - t))
		exp_2_a_T_m_t = tf.pow(exp_a_T_m_t, 2)
		sum_X = tf.reduce_sum(X, -1, keep_dims=True)
		sum_Z = tf.reduce_sum(Z, -1, keep_dims=True)
		cos_sum_X = tf.cos(sum_X)
		sin_sum_X = tf.sin(sum_X)
		
		tmp1 = cos_sum_X * (self.a + self.sigma_zero**2 / 2.) * exp_a_T_m_t
		tmp2 = sin_sum_X * self.mu_zero * exp_a_T_m_t
		tmp3 = self.r * tf.pow(exp_a_T_m_t, 4) * tf.pow(cos_sum_X * sin_sum_X, 2)
		tmp4 = self.r * tf.pow(tf.maximum(-exp_2_a_T_m_t, tf.minimum(Y * sum_Z / (self.sigma_zero * np.sqrt(self.d)), exp_2_a_T_m_t)), 2)
		
		#~ return self.fmult * (tmp1 + tmp2 - tmp3 + tmp4)
		return tmp1 + tmp2 - tmp3 + tmp4
	
	def f_np(self, t, X, Y, Z):
		
		#~ exp_a_T_m_t = tf.constant(np.exp(self.a * (self.T - t)), dtype=tf.float32)
		exp_a_T_m_t = np.exp(self.a * (self.T - t))
		exp_2_a_T_m_t = np.power(exp_a_T_m_t, 2)
		sum_X = np.sum(X, -1, keepdims=True)
		sum_Z = np.sum(Z, -1, keepdims=True)
		
		#~ print(exp_a_T_m_t)
		#~ print(exp_2_a_T_m_t)
		#~ print(sum_X.shape)
		#~ print(sum_Z.shape)
		
		tmp1 = np.cos(sum_X) * (self.a + self.sigma_zero**2 / 2.) * exp_a_T_m_t
		tmp2 = np.sin(sum_X) * self.mu_zero * exp_a_T_m_t
		tmp3 = self.r * np.power(exp_a_T_m_t, 4) * np.power(np.cos(sum_X) * np.sin(sum_X), 2)
		
		tmpmult = np.multiply(Y, sum_Z)
		
		#~ print("Y.shape", Y.shape)
		#~ print("sum_Z", sum_Z.shape)
		
		#~ print("tmpmult", tmpmult.shape)
		
		tmp4 = self.r * np.power(np.maximum(-exp_2_a_T_m_t, np.minimum(tmpmult / (self.sigma_zero * np.sqrt(self.d)), exp_2_a_T_m_t)), 2)
		
		#~ print(tmp4.shape)
		
		return tmp1 + tmp2 - tmp3 + tmp4
	
	def g_tf(self, X):
		return tf.cos(tf.reduce_sum(X, axis=-1, keep_dims=True))
	
	def g_p_tf(self, X):
		return -tf.sin(tf.reduce_sum(X, axis=-1, keep_dims=True))
	
	def g_np(self, X):
		return np.cos(np.sum(X, axis=-1, keepdims=True))
	
	def reference_solution(self, t, X):
		return np.exp(self.a * (self.T - t)) * np.cos(np.sum(X, axis=-1, keepdims=True))
	
	def reference_gradient(self, t, X):
		d = X.shape[-1]
		Xres = np.reshape(X, (-1, d))
		tmp = np.tile(-np.exp(self.a * (self.T - t)) * np.sin(np.sum(Xres, axis=-1, keepdims=True)), (1, d))
		return np.reshape(tmp, X.shape)

	def reference_gradient_tf(self, t, X):
		d = tf.shape(X)[-1]
		Xres = tf.reshape(X, [-1, d])
		tmp = tf.tile(-tf.cast(tf.exp(self.a * (self.T - t)), tf.float32) * tf.sin(tf.reduce_sum(Xres, axis=-1, keep_dims=True)), [1, d])
		return tf.reshape(tmp, tf.shape(X))

############################################################################################################	
############################################################################################################	
############################################################################################################	



class ToyExample_ysz(SemilinearPDE_ReferenceSolution):
	
	def __init__(self, a_mu_zero, a_sigma_zero, a_d, a_T, a_a, a_r):
		self.mu_zero = a_mu_zero
		self.sigma_zero = a_sigma_zero
		self.d = a_d
		self.T = a_T
		self.a = a_a
		self.r = a_r
	
	def diff_np(self, t, X, delta_t):
		return 0
	
	def vol_np(self, t, X, b):
		return self.sigma_zero * b
	
	def diff_tf(self, t, X, delta_t):
		return 0
	
	def get_next(self, t, X, delta_t, delta_b):
		return X + self.sigma_zero * delta_b
	
	def get_next_tf(self, t, X, delta_t, delta_b):
		return X + self.sigma_zero * delta_b
	
	def vol_tf(self, t, X, b):
		return self.sigma_zero * b
		
	def vol_tf_mT(self, t, X, b):
		return b / self.sigma_zero
	
	def f_tf(self, t, X, Y, Z):
		
		exp_a_T_m_t = tf.exp(self.a * (self.T - t))
		sum_X = tf.reduce_sum(X, -1, keep_dims=True)
		sum_Z = tf.reduce_sum(Z, -1, keep_dims=True)
		cos_sum_X = tf.cos(sum_X)
		sin_sum_X = tf.sin(sum_X)
		
		diffusion = self.mu_zero * sum_Z / (self.sigma_zero * self.d)
		nonlinearity = self.r * Y * tf.maximum(tf.minimum(self.d / (tf.sign(sum_Z) * tf.maximum(tf.abs(sum_Z), 1e-1)), 1/exp_a_T_m_t), 1/(3 * exp_a_T_m_t))
		
		tmp0 = 2 * self.a * sum_X * exp_a_T_m_t
		tmp1 = cos_sum_X * (self.a + self.d * self.sigma_zero**2 / 2.) * exp_a_T_m_t
		tmp2 = - self.mu_zero * (2 - sin_sum_X) * exp_a_T_m_t
		tmp3 = - (self.r / self.sigma_zero) * (2 * sum_X + cos_sum_X) / (2 - sin_sum_X)
		
		phi = tmp0 + tmp1 + tmp2 + tmp3
		
		return diffusion + nonlinearity + phi
	
	def f_np(self, t, X, Y, Z):
		
		exp_a_T_m_t = np.exp(self.a * (self.T - t))
		sum_X = np.sum(X, -1, keepdims=True)
		sum_Z = np.sum(Z, -1, keepdims=True)
		
		diffusion = self.mu_zero * sum_Z / (self.sigma_zero * self.d)
		nonlinearity = self.r * Y * np.maximum(np.minimum(self.d / (np.sign(sum_Z) * np.maximum(np.abs(sum_Z), 1e-1)), 1/exp_a_T_m_t), 1/(3 * exp_a_T_m_t))
		
		tmp0 = 2 * self.a * sum_X * exp_a_T_m_t
		tmp1 = cos_sum_X * (self.a + self.d * self.sigma_zero**2 / 2.) * exp_a_T_m_t
		tmp2 = - self.mu_zero * (2 - sin_sum_X) * exp_a_T_m_t
		tmp3 = - (self.r / self.sigma_zero) * (2 * sum_X + cos_sum_X) / (2 - sin_sum_X)
		
		phi = tmp0 + tmp1 + tmp2 + tmp3
		
		return diffusion + nonlinearity + phi
	
	def g_tf(self, X):
		sum_X = tf.reduce_sum(X, -1, keep_dims=True)
		return 2 * sum_X + tf.cos(sum_X)
	
	def g_np(self, X):
		sum_X = np.sum(X, axis=-1, keepdims=True)
		return 2 * sum_X + np.cos(sum_X)
	
	def reference_solution(self, t, X):
		sum_X = np.sum(X, axis=-1, keepdims=True)
		return np.exp(self.a * (self.T - t)) * (2 * sum_X + np.cos(sum_X))
	
	def reference_gradient(self, t, X):
		d = X.shape[-1]
		Xres = np.reshape(X, (-1, d))
		tmp = np.tile(np.exp(self.a * (self.T - t)) * (2 - np.sin(np.sum(X, axis=-1, keepdims=True))), (1, d))
		return np.reshape(tmp, X.shape)

	def reference_gradient_tf(self, t, X):
		d = tf.shape(X)[-1]
		Xres = tf.reshape(X, [-1, d])
		tmp = tf.tile(tf.cast(tf.exp(self.a * (self.T - t)), tf.float32) * (2 - tf.sin(tf.reduce_sum(X, axis=-1, keep_dims=True))), [1, d])
		return tf.reshape(tmp, tf.shape(X))

############################################################################################################	
############################################################################################################	
############################################################################################################	



#~ class ToyExample_zsy(SemilinearPDE_ReferenceSolution):
	
	#~ def __init__(self, a_mu_zero, a_sigma_zero, a_d, a_T, a_a, a_r):
		#~ self.mu_zero = a_mu_zero
		#~ self.sigma_zero = a_sigma_zero
		#~ self.d = a_d
		#~ self.T = a_T
		#~ self.a = a_a
		#~ self.r = a_r
	
	#~ def diff_np(self, t, X, delta_t):
		#~ return 0
	
	#~ def diff_tf(self, t, X, delta_t):
		#~ return 0
	
	#~ def vol_np(self, t, X, b):
		#~ return self.sigma_zero * b
	
	#~ def vol_tf(self, t, X, b):
		#~ return self.sigma_zero * b
	
	#~ def f_tf(self, t, X, Y, Z):
		
		#~ exp_a_T_m_t = tf.cast(tf.exp(self.a * (self.T - t)), tf.float32)
		#~ sum_X = tf.reduce_sum(X, 1, keep_dims=True)
		#~ sum_Z = tf.reduce_sum(Z, 1, keep_dims=True)
		#~ cos_sum_X = tf.cos(sum_X)
		#~ sin_sum_X = tf.sin(sum_X)
		
		#~ diffusion = self.mu_zero * sum_Z / (self.sigma_zero * self.d)
		#~ nonlinearity = self.r * sum_Z / (self.d * tf.maximum(1/(3 * exp_a_T_m_t), tf.minimum(1/exp_a_T_m_t, Y)))
		
		#~ tmp0 = 2 * self.a * exp_a_T_m_t
		#~ tmp1 = cos_sum_X * (self.a + self.d * self.sigma_zero**2 / 2.) * exp_a_T_m_t
		#~ tmp2 = self.mu_zero * sin_sum_X * exp_a_T_m_t
		#~ tmp3 = self.r * self.sigma_zero * sin_sum_X / (2 + cos_sum_X)
		
		#~ phi = tmp0 + tmp1 + tmp2 + tmp3
		
		#~ return diffusion + nonlinearity + phi
	
	#~ def f_np(self, t, X, Y, Z):
		
		#~ exp_a_T_m_t = np.exp(self.a * (self.T - t))
		#~ sum_X = np.sum(X, 1, keepdims=True)
		#~ sum_Z = np.sum(Z, 1, keepdims=True)
		
		#~ diffusion = self.mu_zero * sum_Z / (self.sigma_zero * self.d)
		#~ nonlinearity = self.r * sum_Z / (self.d * np.maximum(1/(3 * exp_a_T_m_t), np.minimum(1/exp_a_T_m_t, Y)))
		
		#~ tmp0 = 2 * self.a * exp_a_T_m_t
		#~ tmp1 = cos_sum_X * (self.a + self.d * self.sigma_zero**2 / 2.) * exp_a_T_m_t
		#~ tmp2 = self.mu_zero * sin_sum_X * exp_a_T_m_t
		#~ tmp3 = self.r * self.sigma_zero * sin_sum_X / (2 + cos_sum_X)
		
		#~ phi = tmp0 + tmp1 + tmp2 + tmp3
		
		#~ return diffusion + nonlinearity + phi
	
	#~ def g_tf(self, t, X):
		#~ sum_X = tf.reduce_sum(X, 1, keep_dims=True)
		#~ return 2 + tf.cos(sum_X)
	
	#~ def g_np(self, t, X):
		#~ sum_X = np.sum(X, axis=1, keepdims=True)
		#~ return 2 + np.cos(sum_X)
	
	#~ def reference_solution(self, t, X):
		#~ sum_X = np.sum(X, axis=1, keepdims=True)
		#~ return np.exp(self.a * (self.T - t)) * (2 + np.cos(sum_X))
	
	#~ def reference_gradient(self, t, X):
		#~ return np.tile(np.exp(self.a * (self.T - t)) * (- np.sin(np.sum(X, axis=1, keepdims=True))), (1, X.shape[1]))

	#~ def reference_gradient_tf(self, t, X):
		#~ return tf.tile(tf.cast(tf.exp(self.a * (self.T - t)), tf.float32) * (- tf.sin(tf.reduce_sum(X, axis=1, keep_dims=True))), [1, tf.shape(X)[1]])


############################################################################################################	
############################################################################################################	
############################################################################################################	


#~ class ToyExample_yz(SemilinearPDE_ReferenceSolution):
	
	#~ def __init__(self, a_mu_zero, a_sigma_zero, a_d, a_T, a_a, a_r):
		#~ self.mu_zero = a_mu_zero
		#~ self.sigma_zero = a_sigma_zero
		#~ self.d = a_d
		#~ self.T = a_T
		#~ self.a = a_a
		#~ self.r = a_r
	
	#~ def diff_np(self, t, X, delta_t):
		#~ return 0
	
	#~ def diff_tf(self, t, X, delta_t):
		#~ return 0
	
	#~ def vol_np(self, t, X, b):
		#~ return self.sigma_zero * b
	
	#~ def vol_tf(self, t, X, b):
		#~ return self.sigma_zero * b
	
	#~ def f_tf(self, t, X, Y, Z):
		
		#~ exp_a_T_m_t = tf.cast(tf.exp(self.a * (self.T - t)), tf.float32)
		#~ sum_X = tf.reduce_sum(X, 1, keep_dims=True)
		#~ sum_Z = tf.reduce_sum(Z, 1, keep_dims=True)
		#~ cos_sum_X = tf.cos(sum_X)
		#~ sin_sum_X = tf.sin(sum_X)
		
		#~ diffusion = self.mu_zero * sum_Z / (self.sigma_zero * self.d)
		#~ nonlinearity = self.r * Y * sum_Z / self.d
		
		#~ tmp0 = 2 * self.a * sum_X * exp_a_T_m_t
		#~ tmp1 = cos_sum_X * (self.a + self.d * self.sigma_zero**2 / 2.) * exp_a_T_m_t
		#~ tmp2 = - self.mu_zero * (2 - sin_sum_X) * exp_a_T_m_t
		#~ tmp3 = - self.r * self.sigma_zero * (2 * sum_X + cos_sum_X) * (2 - sin_sum_X) * exp_a_T_m_t**2
		
		#~ phi = tmp0 + tmp1 + tmp2 + tmp3
		
		#~ return diffusion + nonlinearity + phi
	
	#~ def f_np(self, t, X, Y, Z):
		
		#~ exp_a_T_m_t = np.exp(self.a * (self.T - t))
		#~ sum_X = np.sum(X, 1, keepdims=True)
		#~ sum_Z = np.sum(Z, 1, keepdims=True)
		
		#~ diffusion = self.mu_zero * sum_Z / (self.sigma_zero * self.d)
		#~ nonlinearity = self.r * Y * sum_Z / self.d
		
		#~ tmp0 = 2 * self.a * sum_X * exp_a_T_m_t
		#~ tmp1 = cos_sum_X * (self.a + self.d * self.sigma_zero**2 / 2.) * exp_a_T_m_t
		#~ tmp2 = - self.mu_zero * (2 - sin_sum_X) * exp_a_T_m_t
		#~ tmp3 = - self.r * self.sigma_zero * (2 * sum_X + cos_sum_X) * (2 - sin_sum_X) * exp_a_T_m_t**2
		
		#~ phi = tmp0 + tmp1 + tmp2 + tmp3
		
		#~ return diffusion + nonlinearity + phi
	
	#~ def g_tf(self, t, X):
		#~ sum_X = tf.reduce_sum(X, 1, keep_dims=True)
		#~ return 2 * sum_X + tf.cos(sum_X)
	
	#~ def g_np(self, t, X):
		#~ sum_X = np.sum(X, axis=1, keepdims=True)
		#~ return 2 * sum_X + np.cos(sum_X)
	
	#~ def reference_solution(self, t, X):
		#~ sum_X = np.sum(X, axis=1, keepdims=True)
		#~ return np.exp(self.a * (self.T - t)) * (2 * sum_X + np.cos(sum_X))
	
	#~ def reference_gradient(self, t, X):
		#~ return np.tile(np.exp(self.a * (self.T - t)) * (2 - np.sin(np.sum(X, axis=1, keepdims=True))), (1, X.shape[1]))

	#~ def reference_gradient_tf(self, t, X):
		#~ return tf.tile(tf.cast(tf.exp(self.a * (self.T - t)), tf.float32) * (2 - tf.sin(tf.reduce_sum(X, axis=1, keep_dims=True))), [1, tf.shape(X)[1]])



############################################################################################################	
############################################################################################################	
############################################################################################################


class RichouToyExample(SemilinearPDE_MCSolution):
	
	def __init__(self, a_alpha, a_T):
		self.alpha = a_alpha
		self.T = a_T
	
	def diff_np(self, t, X, delta_t):
		return 0.
		#~ pass
	
	def vol_np(self, t, X, b):
		return b
		#~ pass
	
	def get_next(self, t, X, delta_t, delta_b):
		return X + delta_b
	
	def get_next_tf(self, t, X, delta_t, delta_b):
		return X + delta_b
	
	def vol_tf(self, t, X, b):
		return b
		
	def vol_tf_mT(self, t, X, b):
		return b
		
	def vol_np_mT(self, t, X, b):
		return b
	
	def f_tf(self, t, X, Y, Z):
		return tf.reduce_sum(tf.pow(Z, 2), axis=-1, keep_dims=True) / 2.
	
	def f_np(self, t, X, Y, Z):
		return np.sum(np.power(Z, 2), axis=-1, keepdims=True) / 2.
	
	def g_tf(self, X):
		#~ return tf.pow(tf.maximum(tf.constant(0.), tf.minimum(X, tf.constant(1.))), self.alpha)
		#~ X = tf.where(tf.less(X, 0.), tf.constant([[0.]], tf.float32), tf.where(tf.less(X, 1.), tf.pow(X, self.alpha), tf.constant([[1.]], tf.float32)))
		X = tf.clip_by_value(X, 0., 1.)
		return tf.reduce_sum(tf.pow(X, self.alpha), axis=-1, keep_dims=True)
		#~ return tf.reduce_sum(X, axis=1, keep_dims=True)
	
	def g_np(self, X):
		return np.sum(np.power(np.clip(X, 0., 1.), self.alpha), axis=-1, keepdims=True)
	
	def g_p_np(self, X):
		gp = np.zeros(X.shape)
		idx = np.where(np.logical_and(X > 0, X < 1))
		gp[idx] = self.alpha * np.power(X[idx], self.alpha-1)
		return gp
	
	
	def get_solution_from_file(self, d, n_timesteps, X, override_mc_size=None, override_n_threads=1):
		# Considering a set of parameters, try to read a solution file
		# If nonexisting, compute the solution and save it
		
		# s is a combination of the parameters
		s = str(self.alpha) + str(self.T) + str(d) + str(n_timesteps)
		
		print("s", s)
		m = hashlib.md5()
		m.update(s.encode('utf-8'))
		hashstr = str(m.hexdigest())
		
		print("hashstr", hashstr)
		
		if override_mc_size is None:
			Y_filename = "./mc_solutions/" + "RichouToyExample_Y_" + hashstr
			Z_filename = "./mc_solutions/" + "RichouToyExample_Z_" + hashstr
		else:
			Y_filename = "./mc_solutions/" + "RichouToyExample_Y_" + hashstr + "_" + str(override_mc_size)
			Z_filename = "./mc_solutions/" + "RichouToyExample_Z_" + hashstr + "_" + str(override_mc_size)
		
		nb_realizations = X.shape[0]
		
		# Try to read the file
		Ysol = np.zeros((nb_realizations, n_timesteps+1, 1), dtype=np.float32)
		Zsol = np.zeros((nb_realizations, n_timesteps, d), dtype=np.float32)
		
		try:
			Ysol[:, :, 0] = np.loadtxt(Y_filename, dtype=np.float32)
			for i in range(d):
				Zsol[:, :, i] = np.loadtxt(Z_filename + "_" + str(i), dtype=np.float32)
		except:
			
			print("No available MC solution")
			
		return Ysol, Zsol



############################################################################################################	
############################################################################################################	
############################################################################################################



#~ class CIRToy(SemilinearPDE_ReferenceSolution):
		
	#~ def __init__(self, a, alpha, k, m, sigma, d, T):
		#~ self.a = a
		#~ self.alpha = alpha
		#~ self.T = T,
		#~ self.k = k
		#~ self.m = m
		#~ self.sigma = sigma
		#~ self.d = d
	
	#~ def diff_np(self, t, X, delta_t):
		#~ return self.k * (self.m - X) * delta_t
	
	#~ def vol_np(self, t, X, b):
		#~ return self.sigma * np.multiply(np.sqrt(np.maximum(1e-8, X)), b)
	
	#~ def vol_tf(self, t, X, b):
		#~ return self.sigma * tf.multiply(tf.sqrt(tf.maximum(1e-8, X)), b)
	
	#~ def f_tf(self, t, X, Y, Z):
		#~ sum_X = tf.reduce_sum(X, axis=1, keep_dims=True)
		#~ cos_sum_X = tf.cos(sum_X)
		#~ sin_sum_X = tf.sin(sum_X)
		#~ expTmt = tf.cast(tf.exp(-self.alpha * (self.T - t)), tf.float32)
		
		#~ tmp1 = self.a * Y * tf.reduce_sum(Z, axis=1, keep_dims=True)
		#~ tmp2 = (-self.alpha + self.sigma**2/2 * tf.reduce_sum(X, axis=1, keep_dims=True)) * cos_sum_X * expTmt
		#~ tmp3 = self.k * tf.reduce_sum(self.m - X, axis=1, keep_dims=True) * sin_sum_X * expTmt
		#~ tmp4 = self.a * cos_sum_X * sin_sum_X * expTmt**2 * self.sigma * tf.reduce_sum(tf.sqrt(tf.maximum(1e-8, X)), axis=1, keep_dims=True)
		#~ return tmp1 + tmp2 + tmp3 + tmp4

	#~ def g_tf(self, t, X):
		#~ return tf.cos(tf.reduce_sum(X, 1, keep_dims=True))
	
	#~ def g_np(self, t, X):
		#~ return np.cos(np.sum(X, 1, keepdims=True))

	#~ def reference_solution(self, t, X):
		#~ return np.cos(np.sum(X, 1, keepdims=True)) * np.exp(-self.alpha * (self.T - t))
	
	#~ def reference_gradient(self, t, X):
		#~ return np.tile(-np.sin(np.sum(X, 1, keepdims=True)) * np.exp(-self.alpha * (self.T - t)), (1, X.shape[1]))

	#~ def reference_gradient_tf(self, t, X):
		#~ return tf.tile(-tf.sin(tf.reduce_sum(X, 1, keep_dims=True)) * tf.cast(tf.exp(-self.alpha * (self.T - t)), tf.float32), [1, tf.shape(X)[1]])
