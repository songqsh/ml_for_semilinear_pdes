##
# Implements solvers with different network types
# From https://arxiv.org/abs/1809.07609
#

from deep_nesting.pdes import *
from deep_nesting.solver_base import *

from tensorflow.contrib.layers import fully_connected as fc

############################################################################################################	
############################################################################################################
############################################################################################################	

##
# @brief This corresponds to network C.
class XavierSolver(XavierSolver_base):
	
	def network(self, t, X, reuse):
		# t of shape (N, 1)
		# X of shape (N, d)
		# gX of shape (N, 1)
		
		print(">>>> In network <<<<")
		
		if self.par.normalize_input_X:
			normalized_input_t = (t - self.t_normalizing_mean) / self.t_normalizing_std
			normalized_input_X = (X - self.initial_normalization_means) / self.initial_normalization_stds
		else:
			normalized_input_t = t
			normalized_input_X = X
			
		print("normalized_input_t", normalized_input_t)
		print("normalized_input_X", normalized_input_X)
		
		layer = tf.concat([normalized_input_t, normalized_input_X], axis=-1)
		
		for ell in range(self.par.num_hidden_layers):
			layer = fc(layer, self.par.hidden_layer_size, activation_fn=self.par.activation_fn, scope="layer_" + str(ell), reuse=reuse)
			if ell == 0:
				residual_layer = layer
			elif ell % 2 == 0 or (self.par.num_hidden_layers > 0 and ell == self.par.num_hidden_layers-1):
				layer += residual_layer
		
		# Output layer
		output = fc(layer, 1, activation_fn=None, scope="output_layer", reuse=reuse) #/ self.par.hidden_layer_size
		
		tmpdoutput = tf.gradients(output, X)
		doutput = tmpdoutput[0]
		
		print("tmpdoutput", tmpdoutput)
		
		print(">>>> End in network <<<<")
		
		return output, doutput
		
		
	def compute_u_du_est(
			self, 
			t_expanded, 
			X_expanded, 
			t_inner, 
			tau_tiled,
			brownian_tiled,
			X_prop, 
			XAnt_prop, 
			USim, 
			DUSim, 
			USimAnt, 
			DUSimAnt
			):
			
		# g(t, X)
		gX = self.par.pde.g_tf(X_prop)
		gXAnt = self.par.pde.g_tf(XAnt_prop)
		
		print("gX", gX)
		print("gXAnt", gXAnt)

		try:
			fX = self.par.pde.f_tf_grad(t_inner, X_prop, USim, DUSim)
			fXAnt = self.par.pde.f_tf_grad(t_inner, XAnt_prop, USimAnt, DUSimAnt)
		except:
			fX = self.par.pde.f_tf(t_inner, X_prop, USim, self.par.pde.vol_tf(t_inner, X_prop, DUSim))
			fXAnt = self.par.pde.f_tf(t_inner, XAnt_prop, USimAnt, self.par.pde.vol_tf(t_inner, XAnt_prop, DUSimAnt))
		
		print("fX", fX)
		print("fXAnt", fXAnt)
		
		rho = self.par.lamb * tf.exp(-self.par.lamb * tau_tiled)
		Fbar = tf.exp(-self.par.lamb * tau_tiled)
		
		print("rho", rho)
		print("Fbar", Fbar)
		
		# Estimator of U
		PhigU = 0.5 * (gX + gXAnt) / Fbar
		PhifU = 0.5 * (fX + fXAnt) / rho
		UEst = tf.where(t_inner >= self.par.T, PhigU, PhifU)
		UEst = tf.reduce_mean(UEst, axis=0, keep_dims=False)
		
		weight = tf.divide(self.par.pde.vol_tf_mT(t_expanded, X_expanded, brownian_tiled), tf.sqrt(tau_tiled))
		
		PhigDU = 0.5 * (tf.multiply(gX - gXAnt, weight)) / Fbar
		PhifDU = 0.5 * (tf.multiply(fX - fXAnt, weight)) / rho
		
		DUEst = tf.where(tf.tile(t_inner, [1, 1, self.par.d]) >= self.par.T, PhigDU, PhifDU)
		DUEst = tf.reduce_mean(DUEst, axis=0, keep_dims=False)
		
		print("UEst", UEst)
		print("DUEst", DUEst)
		
		return UEst, DUEst
	
	def get_u_du_est(self, t, X, reuse):
		
		# Number of samples in the outer expectation	
		n_outer = tf.shape(X)[0]
		print("n_outer", n_outer)

		n_inner = self.par.n_inner
		
		# t of shape (n_outer, 1)
		# X of shape (n_outer, d)
		print("t", t)
		print("X", X)
		
		t_expanded = tf.expand_dims(t, 0)
		X_expanded = tf.expand_dims(X, 0)
		
		print("t_expanded", t_expanded)
		print("X_expanded", X_expanded)
		
		# Get t_inner = min(T, t+tau)
		# (n_outer, n_inner, d)
		tmp_tau = tf.tile(self.inner_tau, [1, n_outer, 1])
		t_inner = tf.minimum(self.par.T, t_expanded + tmp_tau)
		
		# Crop tau
		tau_tiled = tf.maximum(t_inner-t_expanded, + 1e-7)
		
		print("t_inner", t_inner)
		print("tau_tiled", tau_tiled)
		
		# Propagate X
		
		brownian_tiled = tf.tile(self.inner_brownian, [1, n_outer, 1])
		dWtau = tf.multiply(brownian_tiled, tf.sqrt(tau_tiled))
		
		X_prop = self.par.pde.get_next_tf(t_expanded, X_expanded, tau_tiled, dWtau) 
		XAnt_prop = self.par.pde.get_next_tf(t_expanded, X_expanded, tau_tiled, -dWtau) 
		
		print("X_prop", X_prop)
		print("XAnt_prop", XAnt_prop)
		
		# Apply neural network
		
		# Do it separately
		
		#~ U, DU = self.network(t_expanded, X_expanded, reuse)
		#~ USim, DUSim = self.network(t_inner, X_prop, reuse)
		#~ USimAnt, DUSimAnt = self.network(t_inner, XAnt_prop, reuse)
		
		# OR
		
		# Combine all the calls into one big call to network
		
		t_net = tf.concat([t_inner, t_inner], axis=0)
		X_net = tf.concat([X_prop, XAnt_prop], axis=0)
		
		U_net, DU_net = self.network(t_net, X_net, reuse)
		
		try:
			self.initialized_u_net
		except:
			self.U, self.DU = self.network(self.t_valid, self.X_valid, True)
			self.initialized_u_net=True
		
		USim, USimAnt = tf.split(U_net, [n_inner, n_inner], axis=0)
		DUSim, DUSimAnt = tf.split(DU_net, [n_inner, n_inner], axis=0)
		
		U, DU = self.network(t_expanded, X_expanded, reuse)
		
		
		print("U", U)
		print("DU", DU)
		print("USim", USim)
		print("DUSim", DUSim)
		print("USimAnt", USimAnt)
		print("DUSimAnt", DUSimAnt)
		
		# Get estimates from Feynman-Kac
		
		UEst, DUEst = self.compute_u_du_est(
			t_expanded=t_expanded, 
			X_expanded=X_expanded, 
			t_inner=t_inner, 
			tau_tiled=tau_tiled,
			brownian_tiled=brownian_tiled,
			X_prop=X_prop, 
			XAnt_prop=XAnt_prop, 
			USim=USim, 
			DUSim=DUSim, 
			USimAnt=USimAnt, 
			DUSimAnt=DUSimAnt
			)
		
		U = tf.squeeze(U, 0)
		DU = tf.squeeze(DU, 0)
		
		print("U", U)
		print("DU", DU)
		print("UEst", UEst)
		print("DUEst", DUEst)
		
		return U, DU, UEst, DUEst
	
	##############
	# NumPy
	##############
	
	
	def compute_u_du_est_np(
			self, 
			t_expanded, 
			X_expanded, 
			t_inner, 
			tau_tiled,
			brownian_tiled,
			X_prop, 
			XAnt_prop, 
			USim, 
			DUSim, 
			USimAnt, 
			DUSimAnt
			):
			
		# g(t, X)
		gX = self.par.pde.g_np(X_prop)
		gXAnt = self.par.pde.g_np(XAnt_prop)
		
		#~ print("gX", gX.shape)
		#~ print("gXAnt", gXAnt.shape)
		
		# f(t, X, u, sigma^T Du)
		try:
			fX = self.par.pde.f_np_grad(t_inner, X_prop, USim, DUSim)
			fXAnt = self.par.pde.f_np_grad(t_inner, XAnt_prop, USimAnt, DUSimAnt)
		except:
			fX = self.par.pde.f_np(t_inner, X_prop, USim, self.par.pde.vol_np(t_inner, X_prop, DUSim))
			fXAnt = self.par.pde.f_np(t_inner, XAnt_prop, USimAnt, self.par.pde.vol_np(t_inner, XAnt_prop, DUSimAnt))
		
		rho = self.par.lamb * np.exp(-self.par.lamb * tau_tiled)
		Fbar = np.exp(-self.par.lamb * tau_tiled)
		
		# Estimator of U
		PhigU = 0.5 * (gX + gXAnt) / Fbar
		PhifU = 0.5 * (fX + fXAnt) / rho
		UEst = np.where(t_inner >= self.par.T, PhigU, PhifU)
		UEst = np.mean(UEst, axis=0, keepdims=False)
		
		weight = np.divide(self.par.pde.vol_np_mT(t_expanded, X_expanded, brownian_tiled), np.sqrt(tau_tiled))

		PhigDU = 0.5 * (np.multiply(gX - gXAnt, weight)) / Fbar
		PhifDU = 0.5 * (np.multiply(fX - fXAnt, weight)) / rho
		
		DUEst = np.where(np.tile(t_inner, (1, 1, self.par.d)) >= self.par.T, PhigDU, PhifDU)
		DUEst = np.mean(DUEst, axis=0, keepdims=False)
		
		return UEst, DUEst
	
	
	##
	# @param t Numpy array
	# @param X Numpy array
	def get_u_du_est_np(self, t, X, n_inner=1000000):
		
		# Number of samples in the outer expectation	
		n_outer = X.shape[0]
		#~ print("n_outer", n_outer)
		
		# Generate tau
		try:
			tau = self.inner_tau_numpy
			brownian = self.inner_brownian_numpy
			if self.inner_tau_numpy.shape[0] != n_inner:
				raise Exception("Incoherent shapes")
		except:
			print("Generating new tau_numpy")
			self.inner_tau_numpy = np.random.exponential(1./self.par.lamb, (n_inner, 1, 1)).astype(np.float32, copy=False)
			self.inner_brownian_numpy = np.random.normal(size=(n_inner, 1, self.par.d)).astype(np.float32, copy=False)
			tau = self.inner_tau_numpy
			brownian = self.inner_brownian_numpy
			print("tau", tau.shape)
			print("brownian", brownian.shape)
		
		#~ print("tau", tau)
		#~ print("brownian", brownian)
		
		# t of shape (n_outer, 1)
		# X of shape (n_outer, d)
		#~ print("t", t)
		#~ print("X", X)
		
		t_expanded = np.expand_dims(t, 0)
		X_expanded = np.expand_dims(X, 0)
		
		# Get t_inner = min(T, t+tau)
		# (n_outer, n_inner, d)
		tmp_tau = np.tile(tau, (1, n_outer, 1))
		t_inner = np.minimum(self.par.T, t_expanded + tmp_tau)
		
		# Crop tau
		tau_tiled = np.maximum(t_inner-t_expanded, + 1e-7)
		
		# Propagate X
		
		brownian_tiled = np.tile(brownian, (1, n_outer, 1))
		dWtau = np.multiply(brownian_tiled, np.sqrt(tau_tiled))
		
		X_prop = self.par.pde.get_next(t_expanded, X_expanded, tau_tiled, dWtau) 
		XAnt_prop = self.par.pde.get_next(t_expanded, X_expanded, tau_tiled, -dWtau) 
		
		# Apply neural network
		
		# Do it separately
		
		# Combine all the calls into one big call to network
		
		t_net = np.concatenate((t_inner, t_inner), axis=0)
		X_net = np.concatenate((X_prop, XAnt_prop), axis=0)
		
		t_net = np.reshape(t_net, (n_outer * (2 * n_inner), 1))
		X_net = np.reshape(X_net, (n_outer * (2 * n_inner), self.par.d))
		
		U_net, DU_net = self.sess.run([self.U, self.DU], feed_dict={self.t_valid:t_net, self.X_valid:X_net})
		
		U_net = np.reshape(U_net, (2 * n_inner, n_outer, 1))
		DU_net = np.reshape(DU_net, (2 * n_inner, n_outer, self.par.d))
		
		tmp = np.split(U_net, [n_inner], axis=0)
		
		USim = tmp[0]
		USimAnt = tmp[1]
		tmp2 = np.split(DU_net, [n_inner], axis=0)
		DUSim = tmp2[0]
		DUSimAnt = tmp2[1]
		
		# Get estimates from Feynman-Kac
		
		UEst, DUEst = self.compute_u_du_est_np(
			t_expanded=t_expanded, 
			X_expanded=X_expanded, 
			t_inner=t_inner, 
			tau_tiled=tau_tiled,
			brownian_tiled=brownian_tiled,
			X_prop=X_prop, 
			XAnt_prop=XAnt_prop, 
			USim=USim, 
			DUSim=DUSim, 
			USimAnt=USimAnt, 
			DUSimAnt=DUSimAnt
			)
		
		return UEst, DUEst
	

	##
	# @brief Compute Y, Z from the given path using the parameters learnt
	#
	# @param self
	# @param X Input path
	# @param dB Input Brownian
	#
	# @return Y Generated Y, shape (nb_realizations, dimension, timesteps)
	# @return Z Generated Z, shape (nb_realizations, dimension, timesteps)
	def run_simulation_from_path(
		self, 
		t,
		X
		):
		
		# Number of realizations
		nb_realizations = X.shape[0]
		
		#~ X_paths = self.get_discretized_paths(nb_realizations, self.par.X0)
		feed_dict = {
			self.t_valid: t,
			self.X_valid: X
		}
		
		Y, Z, loss = self.sess.run([self.test_UEst, self.test_DUEst, self.test_loss],
			feed_dict=feed_dict
			)
		#~ Y, Z = self.get_u_du_est_np(t, X)
		#~ loss = 0.
		
		# Run simulations
		#~ tmp_Y, tmp_Z = self.sess.run([self.test_Y, self.test_Z], 
		#~ Y_path, Z_path, loss = self.sess.run([self.test_Y, self.test_Z, self.test_loss], 
			#~ feed_dict={
				#~ self.X_test: X,
				#~ self.dB_test: dB
			#~ })
		
		return Y, Z, loss


############################################################################################################	
############################################################################################################	
############################################################################################################


##
# @brief Implement XavierSolver without a cost on DU 
# This corresponds to Cbis.
class XavierSolver_NoCostOnDU(XavierSolver):


	def build(self, time, X, training_flag, reuse):
		
		# Compute loss from time, X
		U, DU, UEst, DUEst = self.get_u_du_est(time, X, reuse)
		total_loss = tf.losses.mean_squared_error(U, UEst)
		
		if not training_flag:
			# Compute Y0, Z0
			t0 = tf.constant(0.0, shape=[1, 1], dtype=tf.float32)
			print("t0", self.sess.run(t0))
			
			X0 = tf.constant(self.par.X0, shape=[1, self.par.d], dtype=tf.float32)
			print("X0", self.sess.run(X0))
			
			_, _, Y0, Du0 = self.get_u_du_est(t0, X0, reuse)
			
			Z0 = Du0
			
		else: 
			Y0 = None
			Z0 = None
			
		ZEst = DUEst
		
		return total_loss, UEst, ZEst, Y0, Z0


############################################################################################################	
############################################################################################################	
############################################################################################################


##
# @brief This implements network A.
class XavierSolver_NetworkForDU(XavierSolver):


	def network(self, t, X, reuse):
		# t of shape (N, 1)
		# X of shape (N, d)
		# gX of shape (N, 1)
		
		print(">>>> In network <<<<")
		
		#~ gX = self.par.pde.g_tf(X)
		
		if self.par.normalize_input_X:
			normalized_input_t = (t - self.t_normalizing_mean) / self.t_normalizing_std
			normalized_input_X = (X - self.initial_normalization_means) / self.initial_normalization_stds
		else:
			normalized_input_t = t
			normalized_input_X = X
			
		print("normalized_input_t", normalized_input_t)
		print("normalized_input_X", normalized_input_X)
		
		layer = tf.concat([normalized_input_t, normalized_input_X], axis=-1)
		
		#~ layer = tf.concat([t, X, gX], axis=1)
		
		with tf.variable_scope("network_for_U"):
		
			for ell in range(self.par.num_hidden_layers):
				layer = fc(layer, self.par.hidden_layer_size, activation_fn=self.par.activation_fn, scope="layer_" + str(ell), reuse=reuse)
				
				if ell == 0:
					residual_layer = layer
				elif ell % 2 == 0 or (self.par.num_hidden_layers > 0 and ell == self.par.num_hidden_layers-1):
					layer += residual_layer
			
			# Output layer
			outputU = fc(layer, 1, activation_fn=None, scope="output_layer", reuse=reuse) 
		
		
		with tf.variable_scope("network_for_DU"):
			
			for ell in range(self.par.num_hidden_layers):
				layer = fc(layer, self.par.hidden_layer_size, activation_fn=self.par.activation_fn, scope="layer_" + str(ell), reuse=reuse)
				
				if ell == 0:
					residual_layer = layer
				elif ell % 2 == 0 or (self.par.num_hidden_layers > 0 and ell == self.par.num_hidden_layers-1):
					layer += residual_layer
			
			# Output layer
			outputDU = fc(layer, self.par.d, activation_fn=None, scope="output_layer", reuse=reuse) #/ self.par.hidden_layer_size
		
		print(">>>> End in network <<<<")
		
		return outputU, outputDU



############################################################################################################	
############################################################################################################	
############################################################################################################


##
# @brief This implements network B.
class XavierSolver_NetworkForDU_shared(XavierSolver):


	def network(self, t, X, reuse):
		# t of shape (N, 1)
		# X of shape (N, d)
		# gX of shape (N, 1)
		
		print(">>>> In network <<<<")
		
		#~ gX = self.par.pde.g_tf(X)
		
		if self.par.normalize_input_X:
			normalized_input_t = (t - self.t_normalizing_mean) / self.t_normalizing_std
			normalized_input_X = (X - self.initial_normalization_means) / self.initial_normalization_stds
			#~ normalized_input_gX = (gX - self.gX_normalization_mean) / self.gX_normalization_std
		else:
			normalized_input_t = t
			normalized_input_X = X
			#~ normalized_input_gX = gX
			
		print("normalized_input_t", normalized_input_t)
		print("normalized_input_X", normalized_input_X)
		#~ print("normalized_input_gX", normalized_input_gX)
		
		#~ layer = tf.concat([normalized_input_t, normalized_input_X, normalized_input_gX], axis=-1)
		layer = tf.concat([normalized_input_t, normalized_input_X], axis=-1)
		
		#~ layer = tf.concat([t, X, gX], axis=1)
		
		with tf.variable_scope("network_for_U"):
		
			for ell in range(self.par.num_hidden_layers):
				layer = fc(layer, self.par.hidden_layer_size, activation_fn=self.par.activation_fn, scope="layer_" + str(ell), reuse=reuse)
				
				#~ if ell == 1:
					#~ tf.add_to_collection("checkpoints", layer)
				
				if ell == 0:
					residual_layer = layer
				elif ell % 2 == 0 or (self.par.num_hidden_layers > 0 and ell == self.par.num_hidden_layers-1):
					layer += residual_layer
			
			# Output layer
			output = fc(layer, 1+self.par.d, activation_fn=None, scope="output_layer", reuse=reuse) #/ self.par.hidden_layer_size
		
		#~ with tf.variable_scope("network_for_DU"):
			
			#~ for ell in range(self.par.num_hidden_layers):
				#~ layer = fc(layer, self.par.hidden_layer_size, activation_fn=self.par.activation_fn, scope="layer_" + str(ell), reuse=reuse)
				
				#~ if ell == 0:
					#~ residual_layer = layer
				#~ elif ell % 2 == 0 or (self.par.num_hidden_layers > 0 and ell == self.par.num_hidden_layers-1):
					#~ layer += residual_layer
			
			#~ # Output layer
			#~ outputDU = fc(layer, self.par.d, activation_fn=None, scope="output_layer", reuse=reuse) #/ self.par.hidden_layer_size
		
		
		outputU, outputDU = tf.split(output, [1, self.par.d], axis=-1)
		
		
		print(">>>> End in network <<<<")
		
		return outputU, outputDU



############################################################################################################	
############################################################################################################	
############################################################################################################


##
# @brief This implements a solver Cbis in which DU is not necessary (if f does not depend on DU). It is supposed to be faster (as we do not compute the gradient, we do not put the gradient in the loss either, the TensorFlow graph is smaller)
class XavierSolver_NoDU(XavierSolver_base):
	
	def network(self, t, X, reuse):
		# t of shape (N, 1)
		# X of shape (N, d)
		# gX of shape (N, 1)
		
		print(">>>> In network <<<<")
		
		
		if self.par.normalize_input_X:
			normalized_input_t = (t - self.t_normalizing_mean) / self.t_normalizing_std
			normalized_input_X = (X - self.initial_normalization_means) / self.initial_normalization_stds
		else:
			normalized_input_t = t
			normalized_input_X = X
			
		print("normalized_input_t", normalized_input_t)
		print("normalized_input_X", normalized_input_X)
		
		layer = tf.concat([normalized_input_t, normalized_input_X], axis=-1)
		
		
		for ell in range(self.par.num_hidden_layers):
			layer = fc(layer, self.par.hidden_layer_size, activation_fn=self.par.activation_fn, scope="layer_" + str(ell), reuse=reuse)
			if ell == 0:
				residual_layer = layer
			elif ell % 2 == 0 or (self.par.num_hidden_layers > 0 and ell == self.par.num_hidden_layers-1):
				layer += residual_layer
		
		# Output layer
		output = fc(layer, 1, activation_fn=None, scope="output_layer", reuse=reuse) #/ self.par.hidden_layer_size
		
		print(">>>> End in network <<<<")
		
		return output

	
	def compute_u_du_est(
			self, 
			t_expanded, 
			X_expanded, 
			t_inner, 
			tau_tiled,
			brownian_tiled,
			X_prop, 
			XAnt_prop, 
			USim,
			USimAnt
			):
			
		# g(t, X)
		gX = self.par.pde.g_tf(X_prop)
		gXAnt = self.par.pde.g_tf(XAnt_prop)
		
		print("gX", gX)
		print("gXAnt", gXAnt)
		
		# f(t, X, u, sigma^T Du)
        # Here f does not depend on DU
		fX = self.par.pde.f_tf(t_inner, X_prop, USim)
		fXAnt = self.par.pde.f_tf(t_inner, XAnt_prop, USimAnt)
		
		print("fX", fX)
		print("fXAnt", fXAnt)
		
		rho = self.par.lamb * tf.exp(-self.par.lamb * tau_tiled)
		Fbar = tf.exp(-self.par.lamb * tau_tiled)
		
		print("rho", rho)
		print("Fbar", Fbar)
		
		# Estimator of U
		PhigU = 0.5 * (gX + gXAnt) / Fbar
		PhifU = 0.5 * (fX + fXAnt) / rho
		UEst = tf.where(t_inner >= self.par.T, PhigU, PhifU)
		UEst = tf.reduce_mean(UEst, axis=0, keep_dims=False)
		
		#~ tbis = tf.maximum(Tmt_inner, 1e-8)
		#~ tbis = tau_tiled
		weight = tf.divide(self.par.pde.vol_tf_mT(t_expanded, X_expanded, brownian_tiled), tf.sqrt(tau_tiled)) 
		
		PhigDU = 0.5 * (tf.multiply(gX - gXAnt, weight)) / Fbar
		PhifDU = 0.5 * (tf.multiply(fX - fXAnt, weight)) / rho
		
		DUEst = tf.where(tf.tile(t_inner, [1, 1, self.par.d]) >= self.par.T, PhigDU, PhifDU)
		DUEst = tf.reduce_mean(DUEst, axis=0, keep_dims=False)
		
		print("UEst", UEst)
		print("DUEst", DUEst)
		
		return UEst, DUEst
	
	def get_u_du_est(self, t, X, reuse):
		
		# Number of samples in the outer expectation	
		n_outer = tf.shape(X)[0]
		print("n_outer", n_outer)

		n_inner = self.par.n_inner
		
		tau = self.inner_tau
		brownian = self.inner_brownian
		
		# t of shape (n_outer, 1)
		# X of shape (n_outer, d)
		print("t", t)
		print("X", X)
		
		t_expanded = tf.expand_dims(t, 0)
		X_expanded = tf.expand_dims(X, 0)
		
		print("t_expanded", t_expanded)
		print("X_expanded", X_expanded)
		
		# Get t_inner = min(T, t+tau)
		# (n_outer, n_inner, d)
		tmp_tau = tf.tile(self.inner_tau, [1, n_outer, 1])
		t_inner = tf.minimum(self.par.T, t_expanded + tmp_tau)
		
		# Crop tau
		tau_tiled = tf.maximum(t_inner-t_expanded, + 1e-7)
		
		print("t_inner", t_inner)
		print("tau_tiled", tau_tiled)
		
		# Propagate X
		brownian_tiled = tf.tile(self.inner_brownian, [1, n_outer, 1])
		dWtau = tf.multiply(brownian_tiled, tf.sqrt(tau_tiled))
		
		X_prop = self.par.pde.get_next_tf(t_expanded, X_expanded, tau_tiled, dWtau) 
		XAnt_prop = self.par.pde.get_next_tf(t_expanded, X_expanded, tau_tiled, -dWtau) 
		
		print("X_prop", X_prop)
		print("XAnt_prop", XAnt_prop)
		
		# Apply neural network
		
		# Do it separately
		
		#~ U, DU = self.network(t_expanded, X_expanded, reuse)
		#~ USim, DUSim = self.network(t_inner, X_prop, reuse)
		#~ USimAnt, DUSimAnt = self.network(t_inner, XAnt_prop, reuse)
		
		# OR
		
		# Combine all the calls into one big call to network
		
		t_net = tf.concat([t_expanded, t_inner, t_inner], axis=0)
		X_net = tf.concat([X_expanded, X_prop, XAnt_prop], axis=0)
		
		U_net = self.network(t_net, X_net, reuse)
		
		U, USim, USimAnt = tf.split(U_net, [1, n_inner, n_inner], axis=0)
		
		
		print("U", U)
		print("USim", USim)
		print("USimAnt", USimAnt)
		
		# Get estimates from Feynman-Kac
		
		UEst, DUEst = self.compute_u_du_est(
			t_expanded=t_expanded, 
			X_expanded=X_expanded, 
			t_inner=t_inner, 
			tau_tiled=tau_tiled,
			brownian_tiled=brownian_tiled,
			X_prop=X_prop, 
			XAnt_prop=XAnt_prop, 
			USim=USim,
			USimAnt=USimAnt
			)
		
		U = tf.squeeze(U, 0)
		
		print("U", U)
		#~ print("DU", DU)
		print("UEst", UEst)
		print("DUEst", DUEst)
		
		try:
			self.initialized_u_net
		except:
			self.U = self.network(t, X, True)
			self.initialized_u_net=True
		
		return U, UEst, DUEst
	
	
	def build(self, time, X, training_flag, reuse):
		
		# Compute loss from time, X
		U, UEst, DUEst = self.get_u_du_est(time, X, reuse)
		total_loss = tf.losses.mean_squared_error(U, UEst)
		
		if not training_flag:
			# Compute Y0, Z0
			t0 = tf.constant(0.0, shape=[1, 1], dtype=tf.float32)
			print("t0", self.sess.run(t0))
			
			X0 = tf.constant(self.par.X0, shape=[1, self.par.d], dtype=tf.float32)
			print("X0", self.sess.run(X0))
			
			_, Y0, Du0 = self.get_u_du_est(t0, X0, reuse)
			
			Z0 = Du0
			
		else: 
			Y0 = None
			Z0 = None
			
		ZEst = DUEst
		
		return total_loss, UEst, ZEst, Y0, Z0

	
	##############
	# NumPy
	##############
	
	
	def compute_u_du_est_np(
			self, 
			t_expanded, 
			X_expanded, 
			t_inner, 
			tau_tiled,
			brownian_tiled,
			X_prop, 
			XAnt_prop, 
			USim,
			USimAnt
			):
			
		# g(t, X)
		gX = self.par.pde.g_np(X_prop)
		gXAnt = self.par.pde.g_np(XAnt_prop)
		
		#~ print("gX", gX.shape)
		#~ print("gXAnt", gXAnt.shape)
		
		# f(t, X, u, sigma^T Du)
		try:
			fX = self.par.pde.f_np_grad(t_inner, X_prop, USim)
			fXAnt = self.par.pde.f_np_grad(t_inner, XAnt_prop, USimAnt)
		except:
			fX = self.par.pde.f_np(t_inner, X_prop, USim)
			fXAnt = self.par.pde.f_np(t_inner, XAnt_prop, USimAnt)
		
		#~ print("fX", fX.shape)
		#~ print("fXAnt", fXAnt.shape)
		
		rho = self.par.lamb * np.exp(-self.par.lamb * tau_tiled)
		Fbar = np.exp(-self.par.lamb * tau_tiled)
		
		#~ print("rho", rho)
		#~ print("Fbar", Fbar)
		
		# Estimator of U
		PhigU = 0.5 * (gX + gXAnt) / Fbar
		PhifU = 0.5 * (fX + fXAnt) / rho
		UEst = np.where(t_inner >= self.par.T, PhigU, PhifU)
		UEst = np.mean(UEst, axis=0, keepdims=False)
		
		weight = np.divide(self.par.pde.vol_np_mT(t_expanded, X_expanded, brownian_tiled), np.sqrt(tau_tiled)) # TODO X ???????

		PhigDU = 0.5 * (np.multiply(gX - gXAnt, weight)) / Fbar
		PhifDU = 0.5 * (np.multiply(fX - fXAnt, weight)) / rho
		
		DUEst = np.where(np.tile(t_inner, (1, 1, self.par.d)) >= self.par.T, PhigDU, PhifDU)
		DUEst = np.mean(DUEst, axis=0, keepdims=False)
		
		#~ print("UEst", UEst)
		#~ print("DUEst", DUEst)
		
		return UEst, DUEst
	
	
	##
	# @param t Numpy array
	# @param X Numpy array
	def get_u_du_est_np(self, t, X, n_inner=1000000):
		
		# Number of samples in the outer expectation	
		n_outer = X.shape[0]
		#~ print("n_outer", n_outer)
		
		# Generate tau
		try:
			tau = self.inner_tau_numpy
			brownian = self.inner_brownian_numpy
			if self.inner_tau_numpy.shape[0] != n_inner:
				raise Exception("Incoherent shapes")
		except:
			print("Generating new tau_numpy")
			self.inner_tau_numpy = np.random.exponential(1./self.par.lamb, (n_inner, 1, 1)).astype(np.float32, copy=False)
			self.inner_brownian_numpy = np.random.normal(size=(n_inner, 1, self.par.d)).astype(np.float32, copy=False)
			tau = self.inner_tau_numpy
			brownian = self.inner_brownian_numpy
			print("tau", tau.shape)
			print("brownian", brownian.shape)
		
		#~ print("tau", tau)
		#~ print("brownian", brownian)
		
		# t of shape (n_outer, 1)
		# X of shape (n_outer, d)
		#~ print("t", t)
		#~ print("X", X)
		
		t_expanded = np.expand_dims(t, 0)
		X_expanded = np.expand_dims(X, 0)
		
		#~ print("t_expanded", t_expanded.shape)
		#~ print("X_expanded", X_expanded.shape)
		
		# Get t_inner = min(T, t+tau)
		# (n_outer, n_inner, d)
		tmp_tau = np.tile(tau, (1, n_outer, 1))
		t_inner = np.minimum(self.par.T, t_expanded + tmp_tau)
		
		# Crop tau
		tau_tiled = np.maximum(t_inner-t_expanded, + 1e-7)
		
		#~ print("t_inner", t_inner.shape)
		#~ print("tau_tiled", tau_tiled.shape)
		
		# Propagate X
		
		brownian_tiled = np.tile(brownian, (1, n_outer, 1))
		dWtau = np.multiply(brownian_tiled, np.sqrt(tau_tiled))
		
		X_prop = self.par.pde.get_next(t_expanded, X_expanded, tau_tiled, dWtau) 
		XAnt_prop = self.par.pde.get_next(t_expanded, X_expanded, tau_tiled, -dWtau) 
		
		#~ print("X_prop", X_prop.shape)
		#~ print("XAnt_prop", XAnt_prop.shape)
		
		# Apply neural network
		
		# Do it separately
		
		# Combine all the calls into one big call to network
		
		t_net = np.concatenate((t_inner, t_inner), axis=0)
		X_net = np.concatenate((X_prop, XAnt_prop), axis=0)
		
		t_net = np.reshape(t_net, (n_outer * (2 * n_inner), 1))
		X_net = np.reshape(X_net, (n_outer * (2 * n_inner), self.par.d))
		
		U_net = self.sess.run([self.U], feed_dict={self.t_valid:t_net, self.X_valid:X_net})
		
		U_net = np.reshape(U_net, (2 * n_inner, n_outer, 1))
		
		#~ print("U_net", U_net.shape)
		
		tmp = np.split(U_net, [n_inner], axis=0)
		
		USim = tmp[0]
		USimAnt = tmp[1]
		
		#~ print("USim", USim.shape)
		#~ print("DUSim", DUSim.shape)
		
		# Get estimates from Feynman-Kac
		# This does not use DU
		UEst, DUEst = self.compute_u_du_est_np(
			t_expanded=t_expanded, 
			X_expanded=X_expanded, 
			t_inner=t_inner, 
			tau_tiled=tau_tiled,
			brownian_tiled=brownian_tiled,
			X_prop=X_prop, 
			XAnt_prop=XAnt_prop, 
			USim=USim,
			USimAnt=USimAnt
			)
		
		#~ print("UEst", UEst)
		#~ print("DUEst", DUEst)
		
		return UEst, DUEst
