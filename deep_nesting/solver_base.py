##
# Implements the generic solver for our new algorithm
# From https://arxiv.org/abs/1809.07609
#

from deep_nesting.pdes import *

import time
import os
from os import environ

import matplotlib.pyplot as plt
plt.switch_backend('agg')

from tensorflow.python.tools import inspect_checkpoint as chkp
from tensorflow.python.training.moving_averages import assign_moving_average

from tensorflow.python.ops import math_ops
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import init_ops
from tensorflow.python.ops import nn_ops
from tensorflow.python.ops import variable_scope as vs
from tensorflow.python.util import nest as nest

import multiprocessing
from joblib import Parallel, delayed

import shutil

from tensorflow.contrib import rnn 

from tensorflow.contrib.layers import fully_connected as fc
from tensorflow.contrib.rnn import RNNCell

from tensorflow.contrib.memory_stats.python.ops.memory_stats_ops import BytesInUse

_GRADIENT_CLIPPING_FACTOR = 1.2
_MULTIPROCESSING_CORE_COUNT = multiprocessing.cpu_count()
SWAP_MEMORY = False



def run_from_ipython():
    try:
        __IPYTHON__
        return True
    except NameError:
        return False


print("------------------------------------------------------")
print(" WILL USE " + str(_MULTIPROCESSING_CORE_COUNT) + " THREADS ")
print("------------------------------------------------------")


def run_from_ipython():
    try:
        __IPYTHON__
        return True
    except NameError:
        return False


print("------------------------------------------------------")
print(" RUNNING THE Z := Grad u version ")
print("------------------------------------------------------")
print(" WILL USE " + str(_MULTIPROCESSING_CORE_COUNT) + " THREADS ")
print("------------------------------------------------------")


class XavierSolver_base:
	
	##
	# @brief Subclass that contain the parameters of the simulation: PDE, horizon,
	# discretization, network.
	class Parameters:
	
		def _constant_learning_rate(self, m):
			return self.learning_rate
	
		def __init__(
				self,
				pde,
				d,
				T,
				n_timesteps,
				X0,
				Yini
				):
			
			self.pde = pde
			
			self.d = d
			self.T = T
			
			self.dtype = tf.float32
			
			# Time discretization
			self.n_timesteps = n_timesteps
			self.delta_t = (self.T + 0.0) / self.n_timesteps
			self.tstamps = 0.0 + self.delta_t * np.arange(self.n_timesteps+1)
			
			# Default learning parameters
			self.batch_size = 64
			self.valid_size = 256
			self.n_miniter = 2000
			self.n_maxiter = 100000
			self.n_dispstep = 100
			self.learning_rate = 5e-4
			
			# Define learning rate function
			self.get_learning_rate = self._constant_learning_rate
			self.use_predefined_learning_rate_strategy = False

			self.Yini = Yini

			# Initial condition
			# Should be a line vector (thus reshape)
			self.X0 = np.reshape(X0, (1, self.d))
			
			self.last_loss = 0
			
			# Divide nn by dimension (useful for initialization in AllenCahn)
			self.divide_nn_by_dimension = True
			
			# Normalize X's
			self.normalize_input_X = True
			
			# Normalize Y's
			self.normalize_input_Y_gX = True
			
			self.override_mc=False
			self.compute_reference=True
			
			# Learning rate decrease rule
			self.check_step_count = 1000
			
			# Initializing Y0 around E(g(X_T))?
			self.initialize_Y0_around_gXT = True
			
			# L2 reg amount
			self.l2_regularizer_weight = 0#1e-5
			
			# Xavier parameters
			self.n_inner = 1000
			self.lamb = 1.0
			self.activation_fn = tf.nn.tanh # tf.nn.elu#
			
			# Network parameters
			self.num_hidden_layers = 2
			self.hidden_layer_size = 2 * self.d
			
			self.t_distrib_a = 0.
			
			
	def __init__(
		self, 
		a_parameters, 
		base_name, 
		n_threads=8, 
		seed=None, 
		name_suffix=None, 
		record_session=False, 
		save_session=False,
		output_directory = None
		):
		
		# Name for save paths
		self.base_name = base_name
		self.timestamp = str(int(np.floor(time.time() * 1000)))
		if output_directory is None:
			self.output_directory = "./output/" + self.timestamp + "_" + self.base_name
		else:
			self.output_directory = output_directory
		
		if name_suffix is not None:
			self.output_directory += "_" + name_suffix + "/"
			
		if not os.path.exists(self.output_directory):
			os.makedirs(self.output_directory)
		
		self.par = a_parameters
		
		if seed is not None:
			self.seed = seed
			tf.set_random_seed(self.seed)
		else:
			self.seed = None
		
		self.record_session = record_session
		self.save_session = save_session
		
		self.open()
	
	##
	# @brief Open a new session and create the test network.
	# 
	# @param self 
	def open(self):
		# Set number of threads
		#~ self.sess = tf.Session(config=tf.ConfigProto(intra_op_parallelism_threads=n_threads))
		
		# Or monitor the placement of the tf variables (CPU? GPU?)
		#~ self.sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
		
		# Or control gpu memory growth
		config = tf.ConfigProto()
		
		config.inter_op_parallelism_threads = 2#_MULTIPROCESSING_CORE_COUNT
		self.sess = tf.Session(config=config)
		
		# Or default session
		#~ self.sess = tf.Session()
		
		self.t_normalizing_mean = 0.0
		self.t_normalizing_std = 1.0
		
		if self.par.normalize_input_X or self.par.initialize_Y0_around_gXT:
			
			print("Computing stats on X")
			
			## Get initial normalization means and variance
			nb_initial_normalization_sample = 10000
			_, initial_normalization_sample_paths_X = self.get_discretized_paths(nb_initial_normalization_sample, self.par.X0)
			
			if self.par.normalize_input_X:
				print("Computing normalization mean and variance")
				
				# For easier computation, consider all dimensions are symmetrical and perform computations only on X_0
				self.initial_normalization_means = np.expand_dims(np.mean(initial_normalization_sample_paths_X, axis=(0, 1), keepdims=False), 0)[0, 0]
				self.initial_normalization_stds = np.expand_dims(np.std(initial_normalization_sample_paths_X, axis=(0, 1), keepdims=False), 0)[0, 0]
				print("self.initial_normalization_means", self.initial_normalization_means)
				print("self.initial_normalization_stds", self.initial_normalization_stds)
				
				#~ Xreshaped = np.transpose(initial_normalization_sample_paths_X, (0, 2, 1))
				Xreshaped = np.reshape(initial_normalization_sample_paths_X, (-1, self.par.d))
				gX = self.par.pde.g_np(Xreshaped)
				
				print("gX.shape", gX.shape)
				
				self.gX_normalization_mean = np.mean(gX)
				self.gX_normalization_std = np.std(gX)
				
				print("self.gX_normalization_mean", self.gX_normalization_mean)
				print("self.gX_normalization_std", self.gX_normalization_std)
				
				# Furthermore, normalize t
				# Note that we only give t=0... t=T-deltat to the neural network
				self.t_normalizing_mean = np.mean(self.par.tstamps[:-1])
				self.t_normalizing_std = self.t_normalizing_mean + 1e-8
				print("self.t_normalizing_mean", self.t_normalizing_mean)
				print("self.t_normalizing_std", self.t_normalizing_std)
			
		
		# Generate tau, B
		self.inner_tau = tf.constant(np.random.exponential(1./self.par.lamb,[self.par.n_inner, 1, 1]), tf.float32)
		self.inner_brownian= tf.constant(np.random.normal(size=[self.par.n_inner, 1, self.par.d]), tf.float32)
		
		# Build the test network	
		with tf.variable_scope(self.base_name, reuse=tf.AUTO_REUSE):
		
			# Test placeholders
			self.t_valid = tf.placeholder(tf.float32, (None, 1))
			self.X_valid = tf.placeholder(tf.float32, (None, self.par.d))
	
			self.test_loss, self.test_UEst, self.test_DUEst, self.Y0, self.Z0 = self.build(
				time=self.t_valid, 
				X=self.X_valid, 
				training_flag=False, 
				reuse=tf.AUTO_REUSE
				)
		
		print("self.Y0", self.Y0)
		print("self.Z0", self.Z0)
		
		# Set saver up
		self.saver = tf.train.Saver()
	

	@abc.abstractmethod
	def get_u_du_est(self, time, X, reuse):
		pass
	
	@abc.abstractmethod
	def get_u_du_est_np(self, time, X, n_inner):
		pass
		
	def build(self, time, X, training_flag, reuse):
		
		# Compute loss from time, X
		U, DU, UEst, DUEst = self.get_u_du_est(time, X, reuse)
		total_loss = tf.losses.mean_squared_error(U, UEst) + tf.losses.mean_squared_error(DU, DUEst)
		#~ total_loss = tf.losses.mean_squared_error(U, UEst) + tf.losses.mean_squared_error(DU, DUEst)
		
		if not training_flag:
			# Compute Y0, Z0
			t0 = tf.constant(0.0, shape=[1, 1], dtype=tf.float32)
			print("t0", self.sess.run(t0))
			X0 = tf.constant(self.par.X0, shape=[1, self.par.d], dtype=tf.float32)
			print("X0", self.sess.run(X0))
			
			_, _, Y0, Du0 = self.get_u_du_est(t0, X0, reuse)
			
			Z0 = Du0
		else: 
			Y0 = None
			Z0 = None
			
		ZEst = DUEst
		
		return total_loss, UEst, ZEst, Y0, Z0
		
	##
	# @brief Create a validation sample. Alternate two steps:
	# - training on a batch of paths
	# - validating on the validation sample
	# 
	# @param self 
	# @param run_name Prefix of output files.
	# @param write_output Write output file or not.
	# @param min_decrease_rate Stop training when
	#
	# @return NumPy flexible array with (Iter, Y0, Loss and Runtime)
	def train(
		self, 
		run_name=None, 
		write_output=True, 
		min_decrease_rate=None, 
		nb_max_iter=None
		):
		
		if nb_max_iter is None:
			nb_max_iter = self.par.n_maxiter
		
		self.iter_history = []
		self.loss_history = []
		self.init_history = []
		self.init_grad_history = []
		self.runtime_history = []
		self.learning_rate_history = []
		self.training_loss_history = []
		
		# Validation sample
		start_time = time.time()
		t_valid, X_valid = self.get_sample_paths(self.par.valid_size, np.reshape(self.par.X0, (1, self.par.d)))
		feed_dict_valid = {
			self.t_valid: t_valid,
			self.X_valid: X_valid
		}
		
		
		# Get trainable vars and grads
		start_time = time.time()
		
		
			
		# Sanity check: should work with reuse since no additional variable is created
		with tf.variable_scope(self.base_name, reuse=True):
			
			
			# Define learning rate (can vary between iterations)
			self.learning_rate = tf.placeholder(tf.float32, shape=[], name="learning_rate")
			
			trainable_vars = tf.trainable_variables()
			#~ trainable_vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='')
			print(len(trainable_vars), " variables")
			for i in range(min(30, len(trainable_vars))):
				print(trainable_vars[i])
			#~ trainable_vars = tf.trainable_variables()
			
			for i in range(len(trainable_vars)):
				if i == 0:
					self.trainable_size = tf.size(trainable_vars[i])
				else:
					self.trainable_size += tf.size(trainable_vars[i])
			
			
			
		# This should be in AUTO mode since the gradient variables are overriden from anterior training runs
		with tf.variable_scope(self.base_name, reuse=tf.AUTO_REUSE):
			
			# Define optimizer
			#~ optimizer = tf.contrib.opt.NadamOptimizer(self.learning_rate)
			optimizer = tf.train.AdamOptimizer(self.learning_rate, name='Adam')
			#~ optimizer = tf.train.AdadeltaOptimizer(self.learning_rate)
			
            ## Gradient clipping
			gradients, variables = zip(*optimizer.compute_gradients(self.test_loss, var_list=trainable_vars))
			
			#~ gradients, _ = tf.clip_by_global_norm(gradients, _GRADIENT_CLIPPING_FACTOR * norm_memory)
			sgd_op = optimizer.apply_gradients(zip(gradients, trainable_vars))
			
			# For BN
			update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
			
			self.training_ops = tf.group(*([sgd_op] + update_ops))
			
			adam_vars = [var for var in tf.global_variables() if 'Adam' in var.name]
			
			end_time = time.time()
			print("Optimizer init runtime: ", end_time-start_time, "s")
		
		end_time = time.time()
		
		# Initialize variables
		with tf.variable_scope(self.base_name, reuse=True):
			self.sess.run(tf.global_variables_initializer())
		
		nb_free_param = self.sess.run(self.trainable_size)
		print("Number of free parameters:", nb_free_param)
		
		# Iteration timer
		runtime = 0.
		start_time = time.time()
		
		loss_hist_stop_criterion = []
		
		## Save trainable vars
		var_list = tf.trainable_variables() + tf.get_collection("NOT_TRAINING_VAR_TO_SAVE")

		print("Saving " + str(len(var_list)) + " variables")
		
		training_saver = tf.train.Saver(var_list=var_list)
		savedir = "./saver/" + type(self).__name__ + "_" + type(self.par.network).__name__ + "_" + str(time.time()) + "/"
		if not os.path.exists(savedir):
			os.makedirs(savedir)
		
		# The learning rate is given by a function defined in the parameters
		current_learning_rate = self.par.get_learning_rate(0)
		
		for i in range(nb_max_iter + 1):
			
			# Use predefined learning rate strategy
			if self.par.use_predefined_learning_rate_strategy:
				current_learning_rate = self.par.get_learning_rate(i)
			
			
			t_valid, X_valid = self.get_sample_paths(self.par.batch_size, np.reshape(self.par.X0, (1, self.par.d)))
			
			# Generate training data and iterate
			feed_dict_train = {
				self.t_valid: t_valid,
				self.X_valid: X_valid,
				self.learning_rate : current_learning_rate
			}
			
			
			# Every 100 steps, check test loss
			if i % self.par.n_dispstep == 0:
				
				print("----------------------------------------------------------------")
					
				end_time = time.time()
				rtime = end_time-start_time
				runtime += rtime # do not count the time spent working on the test sample
				
				if i > 0:
					print("Training took %5.3f s" % rtime)
				
				# Evaluate the loss
				start_time = time.time()
				test_loss, Y0, Z00 = self.sess.run([self.test_loss, self.Y0[0, 0], self.Z0[0, 0]], feed_dict=feed_dict_valid)
				end_time = time.time()
				
				rtime = end_time-start_time
				print("Evaluation on the test set took %5.3f s" % rtime)
				

				# Compute mean training loss
				if i == 0:
					training_loss = 0

				
				# Store results
				self.iter_history.append(i)
				self.loss_history.append(test_loss)
				self.init_history.append(Y0)
				self.init_grad_history.append(Z00)  
				self.runtime_history.append(runtime)
				self.learning_rate_history.append(current_learning_rate)
				self.training_loss_history.append(training_loss)
				to_print = "iter={:>8d}  test_loss={:>8.5E}  Y0={:>8.5E}  time={:>8.5f}s  learning_rate={:>8.5f}  Z0dim0={:>8.5f}	training_loss={:>8.5E}".format(i, test_loss, Y0, runtime, current_learning_rate, Z00, training_loss)
				
				loss_hist_stop_criterion.append(test_loss)

				if i == 0 or test_loss < self.best_loss:
					start_time = time.time()
					save_path = training_saver.save(self.sess, savedir + "model.ckpt")
					end_time = time.time()
					rtime = end_time-start_time
					print("Model saved in %5.3f s" % rtime)
					
					to_print += " model saved"
					self.best_loss = test_loss
					
				self.last_loss = test_loss
				
				print(to_print)

				# Every 2000 steps, check test loss
				if not self.par.use_predefined_learning_rate_strategy and min_decrease_rate is not None and i % self.par.check_step_count == 0:
					mean_loss_from_last_check = np.mean(loss_hist_stop_criterion)
					if i != 0:
						# Check if the loss has decreased more than min_decrease_rate in proportion
						decrease_rate = (self.last_loss_check - mean_loss_from_last_check) / self.last_loss_check 
						print("Loss decrease_rate was: ", decrease_rate)
						if decrease_rate < min_decrease_rate:
							if i >= self.par.n_miniter:
								break
							current_learning_rate = np.maximum(1e-6, current_learning_rate / 2.)
					self.last_loss_check = mean_loss_from_last_check
					loss_hist_stop_criterion = []
				
				start_time = time.time()
			
			self.sess.run([self.training_ops], feed_dict=feed_dict_train)
			if i % (self.par.n_dispstep + 1) == 0:
				training_loss = self.sess.run(self.test_loss, feed_dict=feed_dict_train)
		
		# Restore the best model
		training_saver.restore(self.sess, savedir + "model.ckpt")
		print("Restored best variables")
		
		# Remove saving folder
		shutil.rmtree(savedir)
		
		# Request thread stop
		#~ self.coord.request_stop()
		
		# Get best loss index
		best_loss_index = np.argmin(self.loss_history)
		self.best_loss = self.loss_history[best_loss_index]
		self.best_Y0 = self.init_history[best_loss_index]
		self.best_Z0 = self.init_grad_history[best_loss_index]
		self.best_iter = self.iter_history[best_loss_index]
		print("Best loss:", self.best_loss, ", best Y0:", self.best_Y0, ", best Z0:", self.best_Z0, ", iter:", self.best_iter)
		
		base_name = self.output_directory 
		if base_name[-1] != "/":
			base_name += "/"
		np.savetxt(
			base_name + "_best_stats.csv",
			[self.best_loss, self.best_Y0, self.best_Z0, self.best_iter, nb_free_param],
			header="best_loss, best_Y0, best_Z0, best_iter, nb_free_parameters"
		)

		
		# Save results to file
		output_array = np.zeros(len(self.iter_history), dtype=[('Iter', int), ('Loss', float), ('Y0', float), ('Z0', float), ('Runtime', float), ('Learning_rate', float), ('Training_loss', float)])
		output_array["Iter"] = self.iter_history
		output_array["Loss"] = self.loss_history
		output_array["Y0"] = self.init_history
		output_array["Z0"] = self.init_grad_history
		output_array["Runtime"] = self.runtime_history
		output_array["Learning_rate"] = self.learning_rate_history
		output_array["Training_loss"] = self.training_loss_history
		
		if write_output:
			# Get current time as the base name
			base_name = self.output_directory + "/"
			if run_name is not None:
				base_name += run_name
			
			np.savetxt(
					base_name + "_output_array.csv", 
					output_array, 
					fmt="%8d, %8.5E, %8.5E, %8.5E, %8.5f, %8.5f, %8.5E",
					header="Iter, Loss, Y0, Z0, Runtime, Learning_rate, Training_loss"
					)
			# Inspect realizations, plot and save
			self.solution = self.inspect_realizations(suffix_name=run_name)
			
			# Plot loss
			self.plot_and_save_univariate_variable_details(output_array["Iter"], np.expand_dims(output_array["Loss"], axis=0), "Loss", "orange", run_name, xlabel="Iteration", logy=True)
			
			# Plot comp time
			self.plot_and_save_univariate_variable_details(output_array["Iter"], np.expand_dims(output_array["Runtime"], axis=0), "Runtime", "pink", run_name, xlabel="Iteration")
			
			# Plot Y0
			self.plot_and_save_univariate_variable_details(output_array["Iter"], np.expand_dims(output_array["Y0"], axis=0), "Y0", "pink", run_name, xlabel="Iteration")
			
		if self.save_session:
			# Save model
			self.save_model_to_path(base_name + "model.ckpt", global_step=i)
		
		self.output_array = output_array
		
		return output_array
	
	##
	# @brief Saves model (variables) to path
	#
	# @param self
	# @param model_output_path The output path
	# @param global_step Iteration step count
	def save_model_to_path(
		self, 
		model_output_path, 
		global_step=None
		):
		save_path = self.saver.save(self.sess, model_output_path, global_step=global_step)	
	
	##
	# @brief Restores model (variables) from path
	#
	# @param self
	# @param restore_model_path The input path
	def restore_model_from_file(
		self, 
		restore_model_path
		):
		print("Restoring model at path: ", restore_model_path)
		start_time = time.time()
		self.saver.restore(self.sess, restore_model_path)
		end_time = time.time()
		print("Restore completed in ", end_time-start_time, "s")
	
	##
	# @brief Generate sample path X, dB from pde
	# 
	# @param self
	# @param n_path_count Number of paths to be generated
	# @param X0 Initial condition
	#
	# @return X Generated path (NumPy array, shape (nb_realizations, dimension, timesteps))
	# @return dB Generated Brownian (NumPy array, shape (nb_realizations, dimension, timesteps))
	def get_sample_paths(
		self, 
		n_path_count, 
		X0,
		seed=None
		):
		
		# Generate t's
		t = np.random.uniform(0, self.par.T, size=(n_path_count, 1))
		B = np.random.normal(size=(n_path_count, self.par.d)) * np.sqrt(t)
		
		X = self.par.pde.get_next(0, X0, t, B)
		return t.astype(np.float32), X.astype(np.float32)
		
	
	##
	# @brief Generate sample path X, dB from pde
	# 
	# @param self
	# @param n_path_count Number of paths to be generated
	# @param X0 Initial condition
	#
	# @return X Generated path (NumPy array, shape (nb_realizations, dimension, timesteps))
	# @return dB Generated Brownian (NumPy array, shape (nb_realizations, dimension, timesteps))
	def get_discretized_paths(
		self, 
		n_path_count, 
		X0,
		seed=None
		):

		X, _ = self.par.pde.generate_paths(X0, self.par.delta_t, n_path_count, self.par.n_timesteps, seed=seed)
		return np.tile(np.reshape(self.par.tstamps, (1, self.par.n_timesteps+1, 1)), (n_path_count, 1, 1)), X
	
	
	##
	# @brief Compute Y, Z from the given path using the parameters learnt
	#
	# @param self
	# @param X Input path
	# @param dB Input Brownian
	#
	# @return Y Generated Y, shape (nb_realizations, dimension, timesteps)
	# @return Z Generated Z, shape (nb_realizations, dimension, timesteps)
	def run_simulation_from_path(
		self, 
		t,
		X
		):
		
		# Number of realizations
		nb_realizations = X.shape[0]
		
		feed_dict = {
			self.t_valid: t,
			self.X_valid: X
		}
		
		Y, Z, loss = self.sess.run([self.test_UEst, self.test_DUEst, self.test_loss],
			feed_dict=feed_dict
			)
		
		return Y, Z, loss
	
	##
	# @brief Save and plot realization details for a given model
	#
	# @param self
	# @param suffix_name Will be used as a suffix in the output directory.
	#
	# @return output_means A NumPy array containing t and the means of X, Y, Z.
	def inspect_realizations(
		self, 
		suffix_name=None,
		seed=1
		):
		
		nb_realizations = 1500 #10
		n_timesteps = self.par.n_timesteps
		d = self.par.d
		
		# Refined initial condition
		
		t0 = np.zeros((1, 1), np.float32)
		X0 = np.array(self.par.X0, np.float32)
		
		Y0_refined, Z0 = self.get_u_du_est_np(t0, X0, 1000000)
		
		Z0_refined = Z0#self.par.pde.vol_np(t0, X0, Z0)
		
		print("Y0, Z0 computed using a lot of sims")
		print("Y0_refined", Y0_refined)
		print("Z0_refined", Z0_refined)
		
		np.savetxt(
			self.output_directory + "/refined_initial_condition.csv", 
			np.array([Y0_refined, *Z0_refined.reshape(-1).tolist()]),
			header="Y0_refined, Z0_0_refined" 
			)
			
		
		
		# Compute new realizations
		# t of shape (n_timesteps+1, 1)
		# X of shape (nb_realizations, d, n_timesteps+1)
		t, X = self.get_discretized_paths(nb_realizations, np.reshape(self.par.X0, (1, self.par.d)), seed=seed)
		
		print("t.shape", t.shape)
		print("X.shape", X.shape)
		
		# Proceed the paths 10 by 10...
		#~ n_X_slice = 10
		n_X_slice = 3
		print("Evaluating paths " + str(n_X_slice) + " by " + str(n_X_slice))
		N = int(np.ceil(nb_realizations / n_X_slice))
		
		Y_path_list = []
		Z_path_list = []
		
		for i in range(N):
			
			inf_index = i*n_X_slice
			sup_index = np.minimum((i+1)*n_X_slice, nb_realizations)
			slice_length = sup_index - inf_index
			
			# Slice X
			tsliced = t[inf_index:sup_index, :, :]
			Xsliced = X[inf_index:sup_index, :, :]
			
			# Get slice length
			Xslice_length = slice_length
			
			if i % 10 == 0 or i == N-1:
				print("Slice " + str(i) + ", length " + str(Xslice_length))
			
			
			# Reshape X in (Xslice_length * (n_timesteps+1), d)
			tin = np.reshape(tsliced, (Xslice_length * (n_timesteps+1), 1))
			Xin = np.reshape(Xsliced, (Xslice_length * (n_timesteps+1), d))
			
			
			if False and i == 0:
				print("Xin[:(n_timesteps+1), 0]", Xin[:(n_timesteps+1), 0])
				print("tin[:(n_timesteps+1), 0]", tin[:(n_timesteps+1), 0])
				print("Xin", Xin)
				print("tin", tin)
			
			
			Y_path, Z_path, test_loss = self.run_simulation_from_path(tin, Xin)
			
			# Reshape Y to (Xslice_length, n_timesteps+1, 1)
			# Reshape Z to (Xslice_length, n_timesteps+1, d)
			Y_path = np.reshape(Y_path, (Xslice_length, n_timesteps+1, 1))
			Z_path = np.reshape(Z_path, (Xslice_length, n_timesteps+1, d))
			
			
			
			if False and i == 0:
				# Reshape X (debug)
				X_fin = np.reshape(Xin, (Xslice_length, n_timesteps+1, d))
				
				print("Y_path", Y_path)
				print("Z_path", Z_path)
				
				
				# Transpose X (debug)
				print("This shoud be coherent with some realization of X_0", X_fin[-1, :, 0])
				print("This shoud be coherent with some realization of Y", Y_path[-1, :, 0])
				print("This shoud be all the same X_0", X_fin [:, 0, 0])
				
				## DEBUG
				Y0_find = Y_path[0, 0, 0]
				print("Indices where we find Y0_find", np.argwhere(Y_path == Y0_find))
				print("This shoud be all the same Y_0", Y_path[:, 0, 0])
				
				
			# Discard the last Z (for consistency with other methods)
			Z_path = Z_path[:, :-1, :]
			
			Y_path_list.append(Y_path)
			Z_path_list.append(Z_path)
		
		Y_path = np.concatenate(Y_path_list, axis=0)
		Z_path = np.concatenate(Z_path_list, axis=0)
		
		print("Y_path.shape", Y_path.shape)
		print("Z_path.shape", Z_path.shape)
		
		# Final test loss
		np.savetxt(
			self.output_directory + "/final_test_loss.csv", 
			np.array([test_loss]),
			header="test_loss" 
			)
		
		print("Y_path.shape", Y_path.shape)
		print("Z_path.shape", Z_path.shape)
		
		Xplot = X
		
		# Plot X
		X_data = self.plot_and_save_univariate_variable_details(self.par.tstamps, Xplot[:, :, 0], "X (dim 0)", "blue", suffix_name)
		
		# Plot Y
		Y_data = self.plot_and_save_univariate_variable_details(self.par.tstamps, Y_path[:, :, 0], "Y", "red", suffix_name)
		
		# Plot Z
		Z_data = self.plot_and_save_univariate_variable_details(self.par.tstamps, Z_path[:, :, 0], "Z", "green", suffix_name)
		
		# Plot Y - g(X)
		gX = self.par.pde.g_np(Xplot)
		diffYG = Y_path - gX
		diffYG_data = self.plot_and_save_univariate_variable_details(self.par.tstamps, diffYG[:, :, 0], "Y - g(X)", "purple", suffix_name)
		
		# Plot histogram of Y_T - g(X_T)
		fig = plt.figure()
		plt.hist(diffYG[:, -1, 0], bins=100, histtype="stepfilled")
		plt.xlabel("Y_T - g(X_T)")
		plt.title("Distribution of Y_T - g(X_T) (" + str(nb_realizations) + "samples)")
		# Save figure
		fig.savefig(self.output_directory + "/" + "hist_YmGX" + "_plot.pdf")
		if run_from_ipython():
			plt.show()
		
		# Plot trajectories
		fig = plt.figure()
		for i in range(min(diffYG.shape[0], 10)):
			plt.plot(self.par.tstamps, diffYG[i, :, 0])
		plt.xlabel("t")
		plt.ylabel("Y_t - g(X_t)")
		plt.title("Sample trajectories")
		fig.savefig(self.output_directory + "/" + "traj_YmG" + "_plot.pdf")
		if run_from_ipython():
			plt.show()
		
		
		# Compute MC sol with a lot of samples for the trajectories to plot
		n_traj_to_plot = 10
		
		
		# If exact solution is known, get the L1 and L2 norm + sample solutions
		if self.par.compute_reference and (issubclass(type(self.par.pde), SemilinearPDE_ReferenceSolution) or issubclass(type(self.par.pde), SemilinearPDE_MCSolution)):
			
			print("Plotting sample...")
			start_time = time.time()
			
			if issubclass(type(self.par.pde), SemilinearPDE_ReferenceSolution):
				# Get exact solution
				Ysol = np.zeros((nb_realizations, self.par.n_timesteps+1, 1))
				Zsol = np.zeros((nb_realizations, self.par.n_timesteps, self.par.d))
				
				n_threads=_MULTIPROCESSING_CORE_COUNT
				print("n_threads", n_threads)
				
				def computesol(i):
					t = self.par.tstamps[i]
					return self.par.pde.reference_solution(t, Xplot[:, i, :])
				
				for i in range(self.par.n_timesteps+1):
					Ysol[:, i, :] = computesol(i) #res[i]
				
				def computegrad(i):
					t = self.par.tstamps[i]
					return self.par.pde.reference_gradient(t, Xplot[:, i, :])

				
				for i in range(self.par.n_timesteps):
					Zsol[:, i, :] = computegrad(i) #res[i]
			
			if issubclass(type(self.par.pde), SemilinearPDE_MCSolution):
				
				if not self.par.override_mc:
					Ysol, Zsol = self.par.pde.get_solution_from_file(self.par.d, self.par.n_timesteps, Xplot)
				else:
					Ysol, Zsol = self.par.pde.get_solution_from_file(self.par.d, self.par.n_timesteps, Xplot, override_mc_size=self.par.override_mc_size, override_n_threads=self.par.override_n_threads)
				
				
			
			print("Ysol[:, 0, 0]", Ysol[:, 0, 0])
			print("Y_path[:, 0, 0]", Y_path[:, 0, 0])
			
			# Get the errors
			Y0err = Y_path[0, 0, 0]-Ysol[0, 0, 0]
			
			YL1err =  np.abs(Ysol-Y_path)
			int_YL1err = self.par.delta_t * (np.sum(YL1err[:, 1:-1], axis=1) + (YL1err[:, 1] + YL1err[:, -1]) / 2)
			quantile_5_int_YL1err = np.percentile(int_YL1err, 5, axis=0).reshape(-1)
			quantile_50_int_YL1err = np.percentile(int_YL1err, 50, axis=0).reshape(-1)
			quantile_95_int_YL1err = np.percentile(int_YL1err, 95, axis=0).reshape(-1)
			mean_int_YL1err = np.mean(int_YL1err)
			
			ZL2err = np.power(np.linalg.norm(Zsol-Z_path, axis=-1, ord=2, keepdims=True), 2)
			int_ZL2err = self.par.delta_t * (np.sum(ZL2err[:, 1:-1], axis=1) + (ZL2err[:, 1] + ZL2err[:, -1]) / 2)
			quantile_5_int_ZL2err = np.percentile(int_ZL2err, 5, axis=0).reshape(-1)
			quantile_50_int_ZL2err = np.percentile(int_ZL2err, 50, axis=0).reshape(-1)
			quantile_95_int_ZL2err = np.percentile(int_ZL2err, 95, axis=0).reshape(-1)
			mean_int_ZL2err = np.mean(int_ZL2err)
			
			ZL2normsol = np.power(np.linalg.norm(Zsol, axis=-1, ord=2, keepdims=True), 2)
			
			print("int_YL1err", int_YL1err)
			print("int_ZL2err", int_ZL2err)
			
			print("Y0err", Y0err)
			print("mean_int_YL1err", mean_int_YL1err)
			print("mean_int_ZL2err", mean_int_ZL2err)
			
			# Compute the Y0 if f==0
			print("Y0_f", Ysol[0, 0, 0])
			Y0_fnull = np.mean(self.par.pde.g_np(Xplot[:, -1, :]), axis=0)[0]
			print("Y0_fnull", Y0_fnull)
			
			# Compute the error on Z0_0
			Z00err = Z_path[0, 0, 0]-Zsol[0, 0, 0]
			
			# Compute the L2 error on Z0
			Z0sol_l2_norm = np.linalg.norm(Zsol[0, 0, :], ord=2)
			Z0_l2_err_norm = np.linalg.norm(Z_path[0, 0, :] - Zsol[0, 0, :], ord=2)
			
			print("Z0sol_l2_norm", Z0sol_l2_norm)
			print("Z0_l2_err_norm", Z0_l2_err_norm)
			
			np.savetxt(
				self.output_directory + "/losses.csv", 
				np.array([
						Y0err, 
						mean_int_YL1err, 
						quantile_5_int_YL1err, 
						quantile_50_int_YL1err, 
						quantile_95_int_YL1err, 
						mean_int_ZL2err, 
						quantile_5_int_ZL2err, 
						quantile_50_int_ZL2err, 
						quantile_95_int_ZL2err,
						Z00err,
						Ysol[0, 0, 0],
						Zsol[0, 0, 0],
						Z0_l2_err_norm,
						Z0sol_l2_norm
						]),
				header="Y0err, mean_int_YL1err, quantile_5_int_YL1err, quantile_50_int_YL1err, quantile_95_int_YL1err, mean_int_ZL2err, quantile_5_int_ZL2err, quantile_50_int_ZL2err, quantile_95_int_ZL2err, Z00err, Y0ref, Z00ref, Z0_l2_err_norm, Z0sol_l2_norm" 
				)
			
			print("Saving Z0")
			tmp = np.stack((Z_path[0, 0, :], Zsol[0, 0, :]), axis=0)
			print(tmp)
			np.savetxt(
				self.output_directory + "/Z0_ref_sol.csv",
				tmp
				)
			
			#####
			# Compute L2 refined error
			#####
			error_Z0_L2_lot_of_samples = np.linalg.norm(Z0_refined - Zsol[0, 0, :], ord=2)
			np.savetxt(
				self.output_directory + "/initial_L2_error_Z_lot_of_samples.csv", 
				np.array([error_Z0_L2_lot_of_samples]),
				header="error_Z0_L2_lot_of_samples" 
				)
			
			# Plot L1 error
			self.plot_and_save_univariate_variable_details(self.par.tstamps, YL1err[:, :, 0], "L1_error_Y", "red", "L1_error_Y", "Time", False, plot_std=True)
			
			# Plot relative error
			self.plot_and_save_univariate_variable_details(self.par.tstamps, YL1err[:, :, 0] / np.maximum(np.abs(Ysol[:, :, 0]), 1e-8), "Relative_error_Y", "red", "Relative_error_Y", "Time", False, plot_std=False)
			
			# Plot L2 error
			self.plot_and_save_univariate_variable_details(self.par.tstamps, ZL2err[:, :, 0], "L2_error_Z", "purple", "L2_error_Z", "Time", False, plot_std=True)
			
			# Plot relative error
			self.plot_and_save_univariate_variable_details(self.par.tstamps, ZL2err[:, :, 0] / np.maximum(ZL2normsol[:, :, 0], 1e-8), "Relative_error_Z", "purple", "Relative_error_Z", "Time", False, plot_std=False)
			
			
			###########
			# Computing some trajectories with high n_inner
			###########
			
			print("Computing refined trajectories for the first " + str(n_traj_to_plot) + " samples")
			
			start_time = time.time()
			
			#~ Y0, Z0 = self.get_u_du_est_np(t0, X0)
			Y_path_refined = np.zeros((n_traj_to_plot, self.par.n_timesteps+1, 1), dtype=np.float32)
			Z_path_refined = np.zeros((n_traj_to_plot, self.par.n_timesteps+1, self.par.d), dtype=np.float32)
			
			treshaped = np.reshape(self.par.tstamps, (self.par.n_timesteps+1, 1))
			
			n_traj_to_compute_concurrently = 30
			n_concurrent_batches = int(np.ceil((self.par.n_timesteps+1)/n_traj_to_compute_concurrently))
			n_inner = 100000
			
			for i in range(n_traj_to_plot):

				xreshaped = np.reshape(Xplot[i], (self.par.n_timesteps+1, self.par.d))
				
				for j in range(n_concurrent_batches):
					
					index_low = j * n_traj_to_compute_concurrently
					index_high = np.minimum((j+1) * n_traj_to_compute_concurrently, self.par.n_timesteps+1)
					
					treshaped_new = treshaped[index_low:index_high, :]
					xreshaped_new = xreshaped[index_low:index_high, :]
					
					tmpY, tmpZ = self.get_u_du_est_np(treshaped_new, xreshaped_new, n_inner)
					Y_path_refined[i, index_low:index_high, :] = tmpY
					Z_path_refined[i, index_low:index_high, :] = tmpZ
				
			
			Z_path_refined = Z_path_refined[:, :self.par.n_timesteps, :]
			
			
			end_time = time.time()
			print("End, took: " + str(end_time-start_time) + " seconds")
			
			print("Computing errors on refined trajectories")
			
			# Get the errors
			Y0err = Y_path_refined[0, 0, 0]-Ysol[0, 0, 0]
			
			YL1err =  np.abs(Y_path_refined-Ysol[:n_traj_to_plot])
			int_YL1err = self.par.delta_t * (np.sum(YL1err[:, 1:-1], axis=1) + (YL1err[:, 1] + YL1err[:, -1]) / 2)
			quantile_5_int_YL1err = np.percentile(int_YL1err, 5, axis=0).reshape(-1)
			quantile_50_int_YL1err = np.percentile(int_YL1err, 50, axis=0).reshape(-1)
			quantile_95_int_YL1err = np.percentile(int_YL1err, 95, axis=0).reshape(-1)
			mean_int_YL1err = np.mean(int_YL1err)
			
			ZL2err = np.power(np.linalg.norm(Z_path_refined-Zsol[:n_traj_to_plot], axis=-1, ord=2, keepdims=True), 2)
			int_ZL2err = self.par.delta_t * (np.sum(ZL2err[:, 1:-1], axis=1) + (ZL2err[:, 1] + ZL2err[:, -1]) / 2)
			quantile_5_int_ZL2err = np.percentile(int_ZL2err, 5, axis=0).reshape(-1)
			quantile_50_int_ZL2err = np.percentile(int_ZL2err, 50, axis=0).reshape(-1)
			quantile_95_int_ZL2err = np.percentile(int_ZL2err, 95, axis=0).reshape(-1)
			mean_int_ZL2err = np.mean(int_ZL2err)
			
			ZL2normsol = np.power(np.linalg.norm(Zsol, axis=-1, ord=2, keepdims=True), 2)
			
			# Plot L1 error
			self.plot_and_save_univariate_variable_details(self.par.tstamps, YL1err[:, :, 0], "L1_error_Y_refined", "red", "L1_error_Y_refined", "Time", False, plot_std=True)
			
			# Plot relative error
			self.plot_and_save_univariate_variable_details(self.par.tstamps, YL1err[:, :, 0] / np.maximum(np.abs(Ysol[:n_traj_to_plot, :, 0]), 1e-8), "Relative_error_Y_refined", "red", "Relative_error_Y_refined", "Time", False, plot_std=False)
			
			# Plot L2 error
			self.plot_and_save_univariate_variable_details(self.par.tstamps, ZL2err[:, :, 0], "L2_error_Z_refined", "purple", "L2_error_Z_refined", "Time", False, plot_std=True)
			
			# Plot relative error
			self.plot_and_save_univariate_variable_details(self.par.tstamps, ZL2err[:, :, 0] / np.maximum(ZL2normsol[:n_traj_to_plot, :, 0], 1e-8), "Relative_error_Z_refined", "purple", "Relative_error_Z_refined", "Time", False, plot_std=False)
			
			print("Y0err_refined", Y0err)
			print("mean_int_YL1err_refined", mean_int_YL1err)
			print("mean_int_ZL2err_refined", mean_int_ZL2err)
			
			
			# Compute the error on Z0_0
			Z00err = Z_path_refined[0, 0, 0]-Zsol[0, 0, 0]
			
			# Compute the L2 error on Z0
			Z0_l2_err_norm = np.linalg.norm(Z_path_refined[0, 0, :] - Zsol[0, 0, :], ord=2)
			Z0_refined_pp_l2_err_norm = np.linalg.norm(Z0_refined - Zsol[0, 0, :], ord=2)
			
			np.savetxt(
				self.output_directory + "/losses_refined.csv", 
				np.array([
						Y0err, 
						mean_int_YL1err, 
						quantile_5_int_YL1err, 
						quantile_50_int_YL1err, 
						quantile_95_int_YL1err, 
						mean_int_ZL2err, 
						quantile_5_int_ZL2err, 
						quantile_50_int_ZL2err, 
						quantile_95_int_ZL2err,
						Z00err,
						Ysol[0, 0, 0],
						Zsol[0, 0, 0],
						Z0_l2_err_norm,
						Z0_refined_pp_l2_err_norm
						]),
				header="Y0err, mean_int_YL1err, quantile_5_int_YL1err, quantile_50_int_YL1err, quantile_95_int_YL1err, mean_int_ZL2err, quantile_5_int_ZL2err, quantile_50_int_ZL2err, quantile_95_int_ZL2err, Z00err, Y0ref, Z00ref, Z0_l2_err_norm, Z0_l2_err_norm_lot_of_samples" 
				)
			
			
			###########
			# Plotting trajectories with high n_inner
			###########
			
			
			# Plot some trajectories
			for i in range(n_traj_to_plot):#(5):
				fig, ax = plt.subplots()
				yline_dbsde, = ax.plot(self.par.tstamps, Y_path[i, :, 0], color="blue")
				yline_sol, = ax.plot(self.par.tstamps, Ysol[i, :, 0], color="purple")
				yline_sol_final, = ax.plot(self.par.tstamps, Y_path_refined[i, :, 0], color="green")
				
				ax2 = ax.twinx()
				zline_dbsde, = ax2.plot(self.par.tstamps[:-1], Z_path[i, :, 0], color="red")
				zline_sol, = ax2.plot(self.par.tstamps[:-1], Zsol[i, :, 0], color="orange")
				zline_sol_final, = ax2.plot(self.par.tstamps[:-1], Z_path_refined[i, :, 0], color="magenta")
				
				ax.legend([yline_dbsde, yline_sol_final, yline_sol, zline_dbsde, zline_sol_final, zline_sol], ["Y DBSDE", "Y DBSDE Final", "Y Exact solution", "Z (dim 0) DBSDE", "Z (dim 0) DBSDE Final", "Z (dim 0) Exact solution"])
				ax.set_title("Plotting Y and Z (" + str(i) + "th sample)")
				ax.set_xlabel("Time")
				ax.set_ylabel("Y")
				ax2.set_ylabel("Z (dim 0)")
				fig.savefig(self.output_directory + "/" + "sample_traj_" + str(i) + "_plot.pdf")
				
				traj = np.zeros((self.par.n_timesteps+1, 4))
				traj[:, 0] = self.par.tstamps
				traj[:, 1] = Y_path[i, :, 0]
				traj[:, 2] = Y_path_refined[i, :, 0]
				traj[:, 3] = Ysol[i, :, 0]
				np.savetxt(
					self.output_directory + "/Y_sample_traj_" + str(i) + ".csv", 
					np.array(traj),
					header="Time, Ypath, Ypath_final, Ysol" 
					)
				
				if run_from_ipython():
					plt.show()
				
				traj = np.zeros((self.par.n_timesteps, 4))
				traj[:, 0] = self.par.tstamps[:-1]
				traj[:, 1] = Z_path[i, :, 0]
				traj[:, 2] = Z_path_refined[i, :, 0]
				traj[:, 3] = Zsol[i, :, 0]
				np.savetxt(
					self.output_directory + "/Z_sample_traj_" + str(i) + ".csv", 
					np.array(traj),
					header="Time, Zpathdim0, Zpathdim0_final, Zsoldim0" 
					)
				
				traj = np.zeros((self.par.n_timesteps+1, 2))
				traj[:, 0] = self.par.tstamps
				traj[:, 1] = X[i, :, 0]
				np.savetxt(
					self.output_directory + "/X_sample_traj_" + str(i) + ".csv", 
					np.array(traj),
					header="Time, Xdim0" 
					)
				
				end_time = time.time()
				print("Done computing sample, elapsed time " + str(end_time-start_time) + "s")
		
		else:
			
			
			###########
			# Computing some trajectories with high n_inner
			###########
			
			print("Computing refined trajectories for the first " + str(n_traj_to_plot) + " samples")
			
			start_time = time.time()
			
			#~ Y0, Z0 = self.get_u_du_est_np(t0, X0)
			Y_path_refined = np.zeros((n_traj_to_plot, self.par.n_timesteps+1, 1), dtype=np.float32)
			Z_path_refined = np.zeros((n_traj_to_plot, self.par.n_timesteps+1, self.par.d), dtype=np.float32)
			
			treshaped = np.reshape(self.par.tstamps, (self.par.n_timesteps+1, 1))
			
			n_traj_to_compute_concurrently = 30
			n_concurrent_batches = int(np.ceil((self.par.n_timesteps+1)/n_traj_to_compute_concurrently))
			n_inner = 100000
			
			for i in range(n_traj_to_plot):

				xreshaped = np.reshape(Xplot[i], (self.par.n_timesteps+1, self.par.d))
				
				for j in range(n_concurrent_batches):
					
					index_low = j * n_traj_to_compute_concurrently
					index_high = np.minimum((j+1) * n_traj_to_compute_concurrently, self.par.n_timesteps+1)
					
					treshaped_new = treshaped[index_low:index_high, :]
					xreshaped_new = xreshaped[index_low:index_high, :]
					
					# Case in which the slice makes the first dimension disappear
					#~ if index_high - index_low <= 1:
						#~ treshaped_new = np.expand_dims(treshaped_new, 0)
						#~ xreshaped_new = np.expand_dims(xreshaped_new, 0)
					
					tmpY, tmpZ = self.get_u_du_est_np(treshaped_new, xreshaped_new, n_inner)
					#~ tmpZ = self.par.pde.vol_np(treshaped_new, xreshaped_new, tmpZ)
					Y_path_refined[i, index_low:index_high, :] = tmpY
					Z_path_refined[i, index_low:index_high, :] = tmpZ
				
				#~ tmpY, tmpZ = self.get_u_du_est_np(treshaped, xreshaped, n_inner)
				#~ tmpZ = self.par.pde.vol_np(treshaped, xreshaped, tmpZ)
				#~ Y_path_refined[i] = tmpY
				#~ Z_path_refined[i] = tmpZ
			
			Z_path_refined = Z_path_refined[:, :self.par.n_timesteps, :]
			
			#~ for i in range(n_traj_to_plot):
				#~ for j in range(len(self.par.tstamps)):
					#~ t = self.par.tstamps[j]
					#~ tmpY, tmpZ = self.get_u_du_est_np(np.reshape(t, (1, 1)), np.reshape(Xplot[i, j, :], (1, self.par.d)), n_inner)
					#~ Y_path_refined[i, j, :] = tmpY
					#~ if j < self.par.n_timesteps:
						#~ Z_path_refined[i, j, :] = tmpZ
			
			end_time = time.time()
			print("End, took: " + str(end_time-start_time) + " seconds")
			
			
			# Plot Y
			Y_data = self.plot_and_save_univariate_variable_details(self.par.tstamps, Y_path_refined[:, :, 0], "Y refined", "red", suffix_name)
			
			# Plot Z
			Z_data = self.plot_and_save_univariate_variable_details(self.par.tstamps, Z_path_refined[:, :, 0], "Z refined", "green", suffix_name)
			
			# Plot Y - g(X)
			gX = self.par.pde.g_np(Xplot[:n_traj_to_plot])
			diffYG = Y_path_refined - gX
			diffYG_data = self.plot_and_save_univariate_variable_details(self.par.tstamps, diffYG[:, :, 0], "Y - g(X) refined", "purple", suffix_name)
			
			# Plot histogram of Y_T - g(X_T)
			fig = plt.figure()
			plt.hist(diffYG[:, -1, 0], bins=100, histtype="stepfilled")
			plt.xlabel("Y_T - g(X_T)")
			plt.title("Distribution of Y_T - g(X_T) (" + str(nb_realizations) + "samples)")
			# Save figure
			fig.savefig(self.output_directory + "/" + "hist_YmGX_refined" + "_plot.pdf")
			if run_from_ipython():
				plt.show()
			
			# Plot trajectories
			fig = plt.figure()
			for i in range(min(diffYG.shape[0], 10)):
				plt.plot(self.par.tstamps, diffYG[i, :, 0])
			plt.xlabel("t")
			plt.ylabel("Y_t - g(X_t)")
			plt.title("Sample trajectories")
			fig.savefig(self.output_directory + "/" + "traj_YmG" + "_plot.pdf")
			if run_from_ipython():
				plt.show()
			
			
			# plot some trajectories without reference
			# Plot some trajectories
			for i in range(n_traj_to_plot):
				fig, ax = plt.subplots()
				yline_dbsde, = ax.plot(self.par.tstamps, Y_path[i, :, 0], color="blue")
				yline_sol_final, = ax.plot(self.par.tstamps, Y_path_refined[i, :, 0], color="green")
				
				ax2 = ax.twinx()
				zline_dbsde, = ax2.plot(self.par.tstamps[:-1], Z_path[i, :, 0], color="red")
				zline_sol_final, = ax2.plot(self.par.tstamps[:-1], Z_path_refined[i, :, 0], color="magenta")
				
				ax.legend([yline_dbsde, yline_sol_final, zline_dbsde, zline_sol_final], ["Y DBSDE", "Y DBSDE refined", "Z (dim 0) DBSDE", "Z (dim 0) DBSDE refined"])
				ax.set_title("Plotting Y and Z (" + str(i) + "th sample)")
				ax.set_xlabel("Time")
				ax.set_ylabel("Y")
				ax2.set_ylabel("Z (dim 0)")
				fig.savefig(self.output_directory + "/" + "sample_traj_" + str(i) + "_plot.pdf")
				
				traj = np.zeros((self.par.n_timesteps+1, 3))
				traj[:, 0] = self.par.tstamps
				traj[:, 1] = Y_path[i, :, 0]
				traj[:, 2] = Y_path_refined[i, :, 0]
				np.savetxt(
					self.output_directory + "/Y_sample_traj_" + str(i) + ".csv", 
					np.array(traj),
					header="Time, Ypath, Ypathrefined" 
					)
				
				if run_from_ipython():
					plt.show()
				
				traj = np.zeros((self.par.n_timesteps, 3))
				traj[:, 0] = self.par.tstamps[:-1]
				traj[:, 1] = Z_path[i, :, 0]
				traj[:, 2] = Z_path_refined[i, :, 0]
				np.savetxt(
					self.output_directory + "/Z_sample_traj_" + str(i) + ".csv", 
					np.array(traj),
					header="Time, Zpathdim0, Zpathrefined" 
					)
				
				traj = np.zeros((self.par.n_timesteps+1, 2))
				traj[:, 0] = self.par.tstamps
				traj[:, 1] = X[i, :, 0]
				np.savetxt(
					self.output_directory + "/X_sample_traj_" + str(i) + ".csv", 
					np.array(traj),
					header="Time, Xdim0" 
					)
			
		# Get means
		output_means = np.zeros((self.par.tstamps.size, 4), np.float32)
		output_means[:, 0] = self.par.tstamps
		output_means[:, 1] = X_data[:, 1]
		output_means[:, 2] = Y_data[:, 1]
		output_means[:self.par.tstamps.size-1, 3] = Z_data[:, 1]
		
		return output_means
		
	##
	# @brief Save and plot details for given variable
	#
	# @param x Time. Should be np.array((time_dim,)).
	# @param variable Univariate variable of interest. Should be np.array((realization_dim, time_dim)).
	# @param name Name of the variable (for plot) - string.
	# @param color Color of the plot (e.g. "blue",...).
	# @param xlabel Label of the x axis.
	# @param logy Should y be log-scaled?
	#
	# @return Array containing (time, means, 5-quantile, 50-quantile, 95-quantile) for each timestep
	def plot_and_save_univariate_variable_details(
		self, 
		x, 
		variable, 
		name, 
		color, 
		suffix_name=None, 
		xlabel="Time", 
		logy=False,
		plot_std=False
		):
		
		# Compute means
		variable_means = np.mean(variable, axis=0).reshape(-1)
		
		# Compute quantiles
		variable_5_quantile = np.percentile(variable, 5, axis=0).reshape(-1)
		variable_95_quantile = np.percentile(variable, 95, axis=0).reshape(-1)
		variable_50_quantile = np.percentile(variable, 50, axis=0).reshape(-1)
		
		# Compute std
		variable_standard_deviation = np.std(variable, axis=0).reshape(-1)
		
		# Get right dimensions for x
		x = x[:variable_5_quantile.size]
		
		## Plot
		fig, ax = plt.subplots()
		
		# Plot means
		means_line, = ax.plot(x, variable_means, color=color)
		
		if plot_std:
			# Plot stds
			std_up, = ax.plot(x, variable_means + 2 * variable_standard_deviation, color=color, ls="dashed")
			#~ std_down, = ax.plot(x, variable_means - 2 * variable_standard_deviation, color=color, ls="dashed")
		
		# Fill 5 -> 95 quantiles
		quantile_area = ax.fill_between(x, variable_5_quantile, variable_95_quantile, where=variable_95_quantile >= variable_5_quantile, facecolor=color, alpha=0.3, interpolate=True)
		
		ax.set_xlabel(xlabel)
		ax.set_ylabel(name)
		if logy:
			ax.set_yscale("log", nonposy='clip')
		
		if plot_std:
			ax.legend([means_line, std_up, quantile_area], ["Mean", "Mean + 2 * std", "5 to 95% quantiles"])
		else:
			ax.legend([means_line, quantile_area], ["Mean", "5 to 95% quantiles"])
		
		# Save data
		if plot_std:
			output_data = np.zeros((*variable_5_quantile.shape, 6), np.float32)
			output_data[:, 0] = x
			output_data[:, 1] = variable_means
			output_data[:, 2] = variable_5_quantile
			output_data[:, 3] = variable_50_quantile
			output_data[:, 4] = variable_95_quantile
			output_data[:, 5] = variable_standard_deviation
			
			output_name = self.output_directory + "/" + name
			if suffix_name is not None:
				output_name += "_" + suffix_name
			
			np.savetxt(
				output_name + "_plot_data.csv", 
				output_data, 
				fmt="%8.5f, %8.5f, %8.5f, %8.5f, %8.5f, %8.5f",
				header="x, Means, 5-quantile, 50-quantile, 95-quantile, std"
				)
		else:
			# Save data
			output_data = np.zeros((*variable_5_quantile.shape, 5), np.float32)
			output_data[:, 0] = x
			output_data[:, 1] = variable_means
			output_data[:, 2] = variable_5_quantile
			output_data[:, 3] = variable_50_quantile
			output_data[:, 4] = variable_95_quantile
			
			output_name = self.output_directory + "/" + name
			if suffix_name is not None:
				output_name += "_" + suffix_name
			
			np.savetxt(
				output_name + "_plot_data.csv", 
				output_data, 
				fmt="%8.5f, %8.5f, %8.5f, %8.5f, %8.5f",
				header="x, Means, 5-quantile, 50-quantile, 95-quantile"
				)
		
		# Save figure
		fig.savefig(output_name + "_plot.pdf")
		
		if run_from_ipython():
			plt.show()
		
		plt.close('all')
		
		return output_data
		
	
	##
	# @brief Closes the session and resets the graph.
	#
	def close(self):
		tf.reset_default_graph()
		self.sess.close()
